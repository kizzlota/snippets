

such kind of search algorithm is the exclusive desire of the customer 

from interrelation.search_queries import *
from interrelation.serializers import *
from profiles.search_queries import *
from django.views.decorators.csrf import csrf_exempt
import json
import yaml
from django.db.models import Q
from interrelation.search_indexes import TaskSearchSerializer, TaskHighSerializer, ****SearchSerializer
from ***.search_indexes import ***SearchSerializer, ***SearchSerializer
from django.core import serializers
from drf_haystack.filters import HaystackAutocompleteFilter, HaystackHighlightFilter
from haystack.inputs import AutoQuery, Exact, Clean
from django.utils.translation import ugettext_lazy as _


class SearchViewSet(HaystackViewSet):
    permission_classes = (IsAuthenticated,)
    index_models = [***, ***, ***]
    serializer_class = [***, ***, ***,
                        ***]

    LIST_SEARCH_LOCATION = ['***', '***', '**', '***']
    LIST_OF_ACTIONS = ['autocomplete', 'search', 'highlight']

    def main_search(self, request):
        user_query = request.data.get('query')
        type = request.data.get('params')
        action = request.data.get('action')
        if not type:
            location_of_search = 'global'
        else:
            location_of_search = type[0]

        user_initial_path = request.META.get('PATH_INFO', None)
        context = {
            'user_initial_path': user_initial_path,
            'user': request.user,
        }
        if isinstance(type, list):
            if len(type) > 1:
                kwa = {'request': request, 'user_query': user_query, 'type': type}
                resp = self.list_location(type, keywords=kwa)
                return Response(resp, status=status.HTTP_200_OK)
            else:
                if '***' in location_of_search:
                    if 'search' in action:
                        responce = self.***(request=request, user_query=user_query, type=type)
                        return Response(responce, status=status.HTTP_200_OK)
                    if 'autocomplete' in action:
                        self.***(request=request, user_query=user_query, type=type)

                elif '***' in location_of_search:
                    if 'search' in action:
                        responce = self.***(request=request, user_query=user_query, type=type)
                        return Response(responce, status=status.HTTP_200_OK)
                    if 'autocomplete' in action:
                        self.***(request=request, user_query=user_query, type=type)
                elif '***' in location_of_search:
                    if 'search' in action:
                        responce = self.***(request=request, user_query=user_query, type=type)
                        return Response(responce, status=status.HTTP_200_OK)
                elif '***' in location_of_search:
                    if 'search' in action:
                        responce = self.***(request=request, user_query=user_query, type=type)
                        return Response(responce, status=status.HTTP_200_OK)
                elif 'global' in location_of_search:
                    if 'search' in action:
                        responce = self.global_search(request=request, user_query=user_query, type=type)
                        return Response(responce, status=status.HTTP_200_OK)

    def list_location(self, *args, **kwargs):
        data_to_response = {}
        parameters = kwargs.get('keywords')
        for location in args:
            if '***' in location:
                responce = self.***(request=parameters.get('request'), user_query=parameters.get('user_query'),
                                          type=parameters.get('type'))
                data_to_response.update({"***": responce})
            if '***' in location:
                responce = self.***(request=parameters.get('request'), user_query=parameters.get('user_query'),
                                            type=parameters.get('type'))
                data_to_response.update({"doc_profile": responce})
            if '***' in location:
                responce = self.***(request=parameters.get('request'), user_query=parameters.get('user_query'),
                                     type=parameters.get('type'))
                data_to_response.update({"***": responce})
        return data_to_response

    def ***(self, request, user_query=None, type=None):
        if user_query:
            if type:
                user_initial_path = request.META.get('PATH_INFO', None)
                context = {
                    'user_initial_path': user_initial_path,
                    'user': request.user,
                }

                elastic_results = SearchQuerySet().models(Treatment).filter(content=user_query, user=request.user)

                if not elastic_results:
                    spelling = SearchQuerySet().models(***).spelling_suggestion(user_query)
                    elastic_results = SearchQuerySet().models(Treatment).filter(content=spelling).filter(
                        user=request.user)

                if not elastic_results:
                    elastic_results = SearchQuerySet().models(***).auto_query(user_query)

                if not elastic_results:
                    spelling = SearchQuerySet().spelling_suggestion(str(user_query))
                    elastic_results = SearchQuerySet().models(***).filter(content=spelling)

                if not elastic_results:
                    return {"ERROR": _("no data matching query")}

                if elastic_results:
                    serializer = ***Serializer(elastic_results, many=True)
                    if serializer:
                        # print(serializer.data)
                        *** = json.dumps(serializer.data)
                        *** = json.loads(awer)
                        status = tyr[0].get('status')
                        if not status == 'Open':
                            return serializer.data
                        else:
                            return {"ERROR": _(
                                "***")}
                else:
                    return {"ERROR": _(***")}

    def ***autocomplete(self, request, user_query=None, type=None):
        if user_query:
            search_results = SearchQuerySet().models(***).filter(autocomplete=user_query)
            serializer = ***(search_results, many=True)
            if serializer:
                return serializer.data
            return {"ERROR": _('Huston we got a problem')}

    def ***(self, request, user_query=None, type=None):
        if user_query:
            if type:
                user_initial_path = request.META.get('PATH_INFO', None)
                context = {
                    'user_initial_path': user_initial_path,
                    'user': request.user,
                }
                elastic_results = SearchQuerySet().models(***).filter(content=user_query,
                                                                       user=request.user.id)
                if not elastic_results:
                    spelling = SearchQuerySet().models(***).spelling_suggestion(user_query)
                    elastic_results = SearchQuerySet().models(***).filter(content=spelling, user=request.user)
                if not elastic_results:
                    elastic_results = SearchQuerySet().models(***).auto_query(user_query).filter(
                        user=request.user)

                if not elastic_results:
                    return {"ERROR": _("no data matching query")}
                if elastic_results:
                    serializer = ***(elastic_results, many=True)
                    if serializer:
                        return serializer.data
                        # return Response(serializer.data, status=status.HTTP_200_OK)
                        # return Response({"ERROR": 'Huston we got a problev'}, status.HTTP_400_BAD_REQUEST)

    def ***(self, request, user_query=None, type=None):
        if user_query:
            if type:
                user_initial_path = request.META.get('PATH_INFO', None)
                context = {
                    'user_initial_path': user_initial_path,
                    'user': request.user,
                }
                elastic_results = SearchQuerySet().models(***).filter(content=Clean(user_query))
                if not elastic_results:
                    elastic_results = SearchQuerySet().models(***).filter(contains=Clean(user_query))
                if not elastic_results:
                    elastic_results = SearchQuerySet().models(***).filter(categories=Clean(user_query))

                if not elastic_results:
                   
                    elastic_results = SearchQuerySet().models(***).auto_query(user_query)

                if not elastic_results:
                    spelling = SearchQuerySet().models(**).spelling_suggestion(user_query)
                    elastic_results = SearchQuerySet().models(***).filter(content=spelling)

                if not elastic_results:
                    return {"ERROR": _("no data matching query")}
                serializer = ***(elastic_results, many=True)
                if serializer:
                    return serializer.data

    def ***_autocomplete(self, request, user_query=None, type=None):
        if user_query:
            search_results = SearchQuerySet().models(***).filter(autocomplete=user_query)
            serializer = ***(search_results, many=True)
            if serializer:
                return serializer.data

    def ***(self, request, user_query=None, type=None):
        if user_query:
            if type:
                user_initial_path = request.META.get('PATH_INFO', None)
                context = {
                    'user_initial_path': user_initial_path,
                    'user': request.user,
                }

                elastic_results = SearchQuerySet().models(***).filter(content=user_query)

                if not elastic_results:
                    elastic_results = SearchQuerySet().models(***).filter(categories=user_query)

                if not elastic_results:
                    spelling = SearchQuerySet().models(***).spelling_suggestion(user_query)
                    elastic_results = SearchQuerySet().models(***).filter(content=spelling)
                if not elastic_results:
                    elastic_results = SearchQuerySet().models(***).auto_query(user_query)
                if not elastic_results:
                    elastic_results = SearchQuerySet().filter(content=AutoQuery(user_query))
                    if not elastic_results:
                        elastic_results.filter(content=Clean(user_query))
                    # if not elastic_results:
                    #     elastic_results.autocomplete()
                    if not elastic_results:
                        elastic_results = SearchQuerySet().models(***).filter(name__contains=user_query)
                if not elastic_results:
                    return {"ERROR": _("no data matching query")}
                serializer = ***(elastic_results, many=True)
                if serializer:
                    return serializer.data

    def ***(self, request, user_query=None, type=None):
        if user_query:
            search_results = SearchQuerySet().models(***).filter(autocomplete=user_query)
            serializer = ***(search_results, many=True)
            if serializer:
                return serializer.data

    def global_search(self, request, user_query=None, type=None):
        user_query = request.data.get('user_query')
        type = request.data.get('params')
        action = request.data.get('action')
        search_res = {}
        search_results = SearchQuerySet().filter(content=Clean(user_query))
        #
        # search1 = SearchQuerySet().models(DocCategories).filter(content=user_query)
        # print([res for res in search1])
        if not search_results:
            search_results = SearchQuerySet().filter(contains=Clean(user_query))
        if not search_results:
            spelling = SearchQuerySet().spelling_suggestion(Clean(user_query))
            search_results = SearchQuerySet().filter(content=spelling)
        if not search_results:
            search_results = SearchQuerySet().auto_query(user_query)
        for res in search_results:
            print(res.__dict__.get('model_name'))
            model_name = res.__dict__.get('model_name')
            if '***' in model_name:
                if search_results.filter(user=request.user.id):
                    serializer**k = ***(res)
                    search_res.update({"task": serializer_***.data})
            if '***' in model_name:
                if search_results.filter(user=request.user.id):
                    serializer_*** = ***(res)
                    search_res.update({"***": serializer***.data})
            '''
                назву моделі треба прочехлити і поправити
            '''
            if '***' in model_name:
                serializer_*** = ***(res)
                search_res.update({'d***': serializer_***.data})

            if '***' in model_name:
                serializer_*** = ***(res)
                search_res.update({'***s': serializer_***s.data})
        return search_res


class MainSearch(viewsets.ViewSet):
    permission_classes = (AllowAny,)

    @staticmethod
    def transorm(obj):
        return obj.__dict__

    LIST_SEARCH_LOCATION = ['**', '**', '**', '**']
    LIST_OF_ACTIONS = ['autocomplete', 'search', 'highlight']

    def searching(self, request):
        user_query = request.data.get('query')
        type = request.data.get('params')
        action = request.data.get('action')
        if not type:
            location_of_search = 'global'
        else:
            location_of_search = type[0]
        user_initial_path = request.META.get('PATH_INFO', None)
        context = {
            'user_initial_path': user_initial_path,
            'user': request.user,
        }

        if isinstance(type, list):
            if len(type) > 1:
                kwa = {'request': request, 'user_query': user_query, 'type': type}
                resp = self.list_liocation(type, keywords=kwa)
                return Response(resp, status=status.HTTP_200_OK)

            else:
                if '***' in location_of_search:
                    if 'search' in action:
                        responce = self.***(request=request, user_query=user_query, type=type)
                        return Response(responce, status=status.HTTP_200_OK)
                    if 'autocomplete' in action:
                        self.***(request=request, user_query=user_query, type=type)

                elif '***' in location_of_search:
                    if 'search' in action:
                        responce = self.***(request=request, user_query=user_query, type=type)
                        return Response(responce, status=status.HTTP_200_OK)
                    if '***' in action:
                        self.doc_profile_autocomplete(request=request, user_query=user_query, type=type)
                elif '***' in location_of_search:
                    if 'search' in action:
                        responce = self.task(request=request, user_query=user_query, type=type)
                        return Response(responce, status=status.HTTP_200_OK)
                elif '***' in location_of_search:
                    if 'search' in action:
                        responce = self.global_search(request=request, user_query=user_query, type=type)
                        return Response(responce, status=status.HTTP_200_OK)

    def list_liocation(self, *args, **kwargs):
        data_to_response = []
        parameters = kwargs.get('keywords')
        for location in args:
            if '***' in location:
                responce = self.treatment(request=parameters.get('request'), user_query=parameters.get('user_query'),
                                          type=parameters.get('type'))
                data_to_response.append(responce)
            if '***' in location:
                responce = self.doc_profile(request=parameters.get('request'), user_query=parameters.get('user_query'),
                                            type=parameters.get('type'))
                data_to_response.append(responce)
            if '***' in location:
                responce = self.***(request=parameters.get('request'), user_query=parameters.get('user_query'),
                                     type=parameters.get('type'))
                data_to_response.append(responce)
        return {'data': str(data_to_response)}

    def ***(self, request, user_query=None, type=None):
        if user_query:
            if type:
                user_initial_path = request.META.get('PATH_INFO', None)
                context = {
                    'user_initial_path': user_initial_path,
                    'user': request.user,
                }

                data_to_return = {}

                elastic_results = SearchQuerySet().models(***).filter(content=user_query,
                                                                            user=request.user.id)

                if not elastic_results:
                    spelling = SearchQuerySet().models(***).spelling_suggestion(user_query)
                    elastic_results = SearchQuerySet().models(***).filter(content=spelling, user=request.user)

                if not elastic_results:
                    elastic_results = SearchQuerySet().models(***).auto_query(user_query).filter(
                        user=request.user)

                if not elastic_results:
                    return Response({"ERROR": _("no data matching query")}, status=status.HTTP_400_BAD_REQUEST)

                if elastic_results:
                    tags_for_search_pk = []
                    tags_for_search_*** = []
                    for result in elastic_results:
                        tranformed_res = self.transorm(result)
                        tags_for_search_pk.append(tranformed_res.get('pk'))
                        tags_for_search***.append(tranformed_res.get('title'))
                        data_to_return.update({'elastic_results': tranformed_res})
                    #
                    *** = ***.objects.filter(Q(relation_to_user=request.user),
                                                                 Q(id__in=tags_for_search_pk) |
                                                                 Q(title__in=tags_for_search_***_title))

                    *** = ***(instance=***_objects, many=True,
                                                               context=context).data
                    data_to_return.update({'***_objects':***_serializer})
                    search = str(data_to_return)
                    return search

    def ****_autocomplete(self, request, user_query=None, type=None):
        if user_query:
            search_results = SearchQuerySet().models(***).filter(autocomplete=user_query)
            data_to_return = []
            for hit in search_results:
                result_content = self.transorm(hit)
                data_to_return.append(result_content)

    def ***(self, request, user_query=None, type=None):
        if user_query:
            if type:
                user_initial_path = request.META.get('PATH_INFO', None)
                context = {
                    'user_initial_path': user_initial_path,
                    'user': request.user,
                }
                data_to_return = {}

                elastic_results = SearchQuerySet().models(***).filter(content=user_query)
                if not elastic_results:
                    search_results = SearchQuerySet().models(***).filter(content=user_query)
                    if search_results:
                        cat_ids = []
                        for result in search_results:
                            tranformed_res = self.transorm(result)
                            data_to_return.update({search_results: tranformed_res})
                            cat_ids.append(int(tranformed_res.get('pk')))
                        *** = ***.objects.filter(***__in=cat_ids)
                        serializer = ***(doctors, many=True, context=context)
                        data_to_return.update({'***': serializer.data})

                if not elastic_results:
                    elastic_results = SearchQuerySet().models(***).filter(categories=user_query)

                if not elastic_results:
                    spelling = SearchQuerySet().models(***).spelling_suggestion(user_query)
                    elastic_results = SearchQuerySet().models(***).filter(content=spelling)

                if not elastic_results:
                    elastic_results = SearchQuerySet().models(***).auto_query(user_query)
                if not elastic_results:
                    return Response({"ERROR": _("no data matching query")}, status=status.HTTP_400_BAD_REQUEST)
                key_list = []
                for result in elastic_results:
                    result_content = self.transorm(result)
                    key_list.append(result_content)

                *** = [keys.get('pk') for keys in key_list]
                profiles = ***.objects.filter(id__in=doctor_profiles)
                serializer = ***(profiles, many=True, context=context).data
                data_to_return.update({'***': serializer})
                search = str(data_to_return)
                return search

    def ***(self, request, user_query=None, type=None):
        if user_query:
            search_results = SearchQuerySet().models(****).filter(autocomplete=user_query)
            data_to_return = []
            for hit in search_results:
                result_content = self.transorm(hit)
                data_to_return.append(result_content)

    def task(self, request, user_query=None, type=None):

        if user_query:
            if type:
                user_initial_path = request.META.get('PATH_INFO', None)
                context = {
                    'user_initial_path': user_initial_path,
                    'user': request.user,
                }
                data_to_return = {}

                elastic_results = SearchQuerySet().models(****).filter(content=user_query,
                                                                       user=request.user.id)
                if not elastic_results:
                    spelling = SearchQuerySet().models(***).spelling_suggestion(user_query)
                    elastic_results = SearchQuerySet().models(***).filter(content=spelling, user=request.user)
                if not elastic_results:
                    elastic_results = SearchQuerySet().models(***).auto_query(user_query).filter(
                        user=request.user)

                if not elastic_results:
                    return {"ERROR": _("no data matching query")}
                if elastic_results:
                    tags_for_search_pk = []
                    for result in elastic_results:
                        tranformed_res = self.transorm(result)
                        tags_for_search_pk.append(tranformed_res.get('pk'))
                        data_to_return.update({'elastic_results': tranformed_res})

                    task_objects = ***.objects.filter(Q(relation_to_***__relation_to_user=request.user),
                                                       Q(id__in=tags_for_search_pk))
                    task_serializer = ***(instance=task_objects, many=True).data
                    data_to_return.update({'***': ***})
                search = str(data_to_return)
                return search

    def global_search(self, request, user_query=None, type=None):
        type = type
        if user_query:
            user_initial_path = request.META.get('PATH_INFO', None)
            context = {
                'user_initial_path': user_initial_path,
                'user': request.user,
            }
            data_to_return = {}
            if request.user.is_authenticated:
                elastic_results = SearchQuerySet().filter(content=user_query)

                if not elastic_results:
                    elastic_results = SearchQuerySet().filter(categories=user_query, user=request.user)

                if not elastic_results:
                    spelling = SearchQuerySet().spelling_suggestion(user_query)
                    elastic_results = SearchQuerySet().filter(content=spelling, user=request.user)

                if not elastic_results:
                    elastic_results = SearchQuerySet().auto_query(user_query)
                if not elastic_results:
                    return Response({"ERROR": _("no data matching query")}, status=status.HTTP_400_BAD_REQUEST)
                key_list = []
                data_to_return = []
                for result in elastic_results:
                    model_name = str(result.__dict__.get('model_name')).capitalize()
                    id = str(result.__dict__.get('pk'))
                    qury = eval(model_name).objects.filter(id=id)
                    data_to_return.append(result.__dict__)
                    # result_content = self.transorm(result)
                    # key_list.append(result_content)
                search = str(data_to_return)
                return search
            else:
                elastic_results = SearchQuerySet().filter(content=user_query)
                if not elastic_results:
                    search_results = SearchQuerySet().filter(content=user_query)
                    if search_results:
                        cat_ids = []
                        for result in search_results:
                            tranformed_res = self.transorm(result)
                            data_to_return.update({search_results: tranformed_res})
                            cat_ids.append(int(tranformed_res.get('pk')))
                        doctors = ***.objects.filter(relation_***__in=cat_ids)
                        serializer = ***(doctors, many=True, context=context)
                        data_to_return.update({'***': serializer.data})

                if not elastic_results:
                    elastic_results = SearchQuerySet().filter(categories=user_query)

                if not elastic_results:
                    spelling = SearchQuerySet().spelling_suggestion(user_query)
                    elastic_results = SearchQuerySet().filter(content=spelling)

                if not elastic_results:
                    elastic_results = SearchQuerySet().auto_query(user_query)
                if not elastic_results:
                    return Response({"ERROR": _("no data matching query")}, status=status.HTTP_400_BAD_REQUEST)
                key_list = []
                for result in elastic_results:
                    result_content = self.transorm(result)
                    key_list.append(result_content)
                    # return search
