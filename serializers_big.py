# coding=utf-8
import re
import base64
from django.contrib.auth import get_user_model
from rest_framework import serializers
from rest_framework.validators import UniqueValidator, ValidationError
from django.contrib.auth import password_validation
from profiles.models import *
from django.core import validators
from versatileimagefield.serializers import VersatileImageFieldSerializer
from django.utils import translation
from django.utils.translation import ugettext_lazy as _
from PIL import Image
from django.db.models import Q
from interrelation.models import Ratings
from django.core.files.base import ContentFile
from generic_relations.relations import GenericRelatedField
import paramiko

class JSONSerializerField(serializers.Field):
    """ Serializer for JSONField -- required to make field writable"""

    def to_internal_value(self, data):
        return data

    def to_representation(self, value):
        return value


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = get_user_model()
        fields = ('id', 'url', 'email')  # 'username',


class EmailCheckSerializer(serializers.Serializer):
    email = serializers.EmailField(
        validators=[
            UniqueValidator(queryset=User.objects.all(),
                            message='User with this Email address already exists.'),
        ]
    )


def query_registration_validator(instance):
    check_user = User.objects.filter(Q(username__iexact=instance))


class AccountCreateSerializer(serializers.Serializer):
    query = serializers.CharField(max_length=100, validators=[validators.RegexValidator(re.compile('^[a-zA-Z0-9@+-]')),
                                                              UniqueValidator(queryset=User.objects.all(),
                                                                              message="exists with same credentials")])


class AccountSerializer(serializers.ModelSerializer):
    '''
    fields validation - username , email & serializing model User. but User model has been modified and used
    rest-framework, for this reason had used --- get_user_model()
    get_user_model() - locale in Python27\Lib\site-packages\django\contrib\auth\__init__.py
    '''
    # query = serializers.CharField(max_length=100, validators=[validators.RegexValidator(re.compile('^[a-zA-Z0-9@+-]')),
    #                                                           UniqueValidator(queryset=User.objects.all(),
    #                                                                           message="exists with same credentials")])
    # username = serializers.CharField(max_length=30, required=False,
    #                                  validators=[
    #                                      validators.RegexValidator(re.compile('^[\w.@+-]+$'), 'Enter a valid username.',
    #                                                                'invalid'),
    #                                      UniqueValidator(queryset=User.objects.all(),
    #                                                      message='This username already exists.')]
    #                                  )
    email = serializers.CharField(max_length=60, required=False, allow_blank=True,
                                  validators=[
                                      UniqueValidator(queryset=User.objects.all(),
                                                      message='User with this Email address already exists.'),
                                      validators.EmailValidator()]
                                  )
    user_photo = serializers.SerializerMethodField(required=False)
    type_registration = serializers.SerializerMethodField(read_only=True, required=False)

    def __init__(self, *args, **kwargs):
        remove_fields = kwargs.pop('remove_fields', None)
        super(AccountSerializer, self).__init__(*args, **kwargs)
        if remove_fields:
            for field in remove_fields:
                self.fields.pop(field)

    def get_type_registration(self, obj):
        return self.context.get('type_registration')

    def create(self, validated_data):
        user = User.objects.create_user(**validated_data)
        return user

    class Meta:
        model = get_user_model()
        fields = ('id', 'phone_number', 'email', 'password', '***', 'first_name', 'second_name', 'last_name',
                  'user_photo', 'language', 'personal_settings', '***', '***', '***',
                  'confirmed')
        read_only_fields = ('user_photo',)

    def get_user_photo(self, obj):
        try:
            instance = UserPhoto.objects.get(relation_to_user__email=obj.email)
        except UserPhoto.DoesNotExist as e:
            return {'ERROR': str(e)}
        photo_link = UserPhotoSerializer(instance=instance, read_only=True)
        return photo_link.data['image']

    def validate_email(self, value):
        if isinstance(value, str):
            email_lower = value.lower()
            return email_lower
        else:
            raise ValidationError('email must be string')

    # def validate_phone_number(self, value):
    #     if value[0] != '+':
    #         raise ValidationError(_('first symbol need to be + in phone number field'))
    #     if len(value) < 10:
    #         raise ValidationError(_('Lenght of phone number must be greater than 10 characters'))

    def to_representation(self, instance):
        # get the original representation
        remove = super(AccountSerializer, self).to_representation(instance)
        # remove fields
        remove.pop('password')
        return remove


class AccountUpdateSerializer(serializers.ModelSerializer):
    # def __init__(self, *args, **kwargs):
    #     remove_fields = kwargs.pop('remove_fields', None)
    #     super(AccountUpdateSerializer, self).__init__(*args, **kwargs)
    #     self.fields.pop(field)
    #

    class Meta:
        model = get_user_model()
        fields = ('id', 'first_name', 'second_name', 'last_name', 'personal_settings', 'language', 'privacy')

    def update(self, instance, validated_data):
        instance.first_name = validated_data.get('first_name', instance.first_name)
        instance.second_name = validated_data.get('second_name', instance.second_name)
        instance.last_name = validated_data.get('last_name', instance.last_name)
        instance.language = validated_data.get('language', instance.language)
        instance.privacy = validated_data.get('privacy', instance.privacy)
        instance.personal_settings = validated_data.get('personal_settings', instance.personal_settings)
        instance.save()
        return instance


class AccountRecoveryEmailSerializer(serializers.Serializer):
    email = serializers.CharField(max_length=30, validators=[validators.EmailValidator()])


class AccountRecoveryPasswSerializer(serializers.Serializer):  # Vova please add some password validation
    password = serializers.CharField(style={'input_type': 'password'}, write_only=True, )

    def validate_password(self, value):
        errors = dict()
        try:
            from django.contrib.auth.password_validation import validate_password
            validated_passs = validate_password(password=value)
        except ValidationError as e:
            errors['password'] = list(e.detail)

        if errors:
            raise serializers.ValidationError(errors)

        return super(AccountRecoveryPasswSerializer, self).validate(value)

class AccountLoginSerializer(serializers.Serializer):
    '''
    serializer used for validate fields
    -------------------------------------------------------------

    '''
    email = serializers.CharField(max_length=30, validators=[validators.EmailValidator()])
    # username = serializers.CharField(max_length=30,
    #                                  validators=[
    #                                      validators.RegexValidator(re.compile('^[\w.@+-]+$'),
    #                                                                'Username format is not valid.',
    #                                                                'invalid')]
    #                                  )

    password = serializers.CharField()


class AuthorPhotoSerializer(serializers.ModelSerializer):
    image = VersatileImageFieldSerializer(sizes=[('medium_square_crop', 'crop__400x400')])

    class Meta:
        model = UserPhoto
        fields = ('image',)


class ImagesEveryoneSerializer(serializers.ModelSerializer):
    image = VersatileImageFieldSerializer(
        sizes=[
            ('full_size', 'url'),
            ('crop', 'crop__1000x800'),
            ('small_square_crop', 'crop__50x50')
        ]
    )

    class Meta:
        model = ImagesEveryone
        fields = ('id', 'name', 'image', 'height', 'width', 'ppoi')
        read_only_fields = ('id',)

    def create(self, validated_data):
        image_instance = ImagesEveryone.objects.create(**validated_data)
        if self.context['user'].is_doc:
            doc_instance = ***.objects.get(relation_to_user=self.context['user'].id)
            doc_instance.relation_to_images.add(image_instance)
        else:
            user_instance = ***.objects.get(relation_to_user=self.context['user'].id)
            user_instance.relation_to_images.add(image_instance)
        return image_instance


class User***Serializer(serializers.ModelSerializer):
    def __init__(self, *args, **kwargs):
        remove_fields = kwargs.pop('remove_fields', None)
        super(***, self).__init__(*args, **kwargs)

        if remove_fields:
            # for multiple fields in a list
            for field_name in remove_fields:
                self.fields.pop(field_name)

    *** = serializers.ChoiceField(allow_blank=True, choices=UserBioDetails.***_CHOICES, required=False)
    *** = serializers.ChoiceField(allow_blank=True, choices=UserBioDetails.***_CHOICES, required=False)
    ** = serializers.ChoiceField(allow_blank=True, choices=UserBioDetails.***_CHOICES, required=False)
    ** = serializers.SerializerMethodField()
    second_name = serializers.SerializerMethodField()
    last_name = serializers.SerializerMethodField()
    last_activity = serializers.SerializerMethodField()
    language = serializers.SerializerMethodField()
    privacy = serializers.SerializerMethodField()
    *** = serializers.SerializerMethodField()
    user_photo = serializers.SerializerMethodField(required=False)
    user_id = serializers.SerializerMethodField(required=False)
    username = serializers.SerializerMethodField(required=False)
    user_rating = serializers.SerializerMethodField()

    class Meta:
        model = UserBioDetails
        fields = (
            'id', 'username', 'user_id', 'user_photo', 'first_name', 'second_name', 'last_name', '***', '***',
            'telephone_number', 'address', '***', '***', '***', '***',
            '***', '***', '***', '***', '***',
            '**', '**', 'height', 'weight', '***', '***', '***',
            '***', '***', 'city', 'street', 'country', 'relation_to_images', 'last_activity',
            'language', 'privacy', 'medical_information', 'user_rating',
        )
        read_only_fields = ('id', 'last_activity')

    def get_user_rating(self, obj):
        rate = Ratings.objects.filter(relation_to_user=obj.relation_to_user)
        if rate:
            rate.first()
            try:
                score = rate.user_ratings_json.get('rating').get('score')
            except AttributeError:
                score = 0
            doc_score = {"score": score}
            return doc_score

    def get_***(self, obj):
        return obj.get_***_***()

    def get_***(self, obj):
        return obj.get_***_***()

    def get_***(self, obj):
        return obj.get_***()

    def get_username(self, obj):
        return obj.relation_to_user.email

    def get_first_name(self, obj):
        if obj.relation_to_user.privacy:
            return _('anonymous')
        else:
            first_name = obj.relation_to_user.first_name
            return first_name

    def get_second_name(self, obj):
        if obj.relation_to_user.privacy:
            return _('anonymous')
        else:
            second_name = obj.relation_to_user.second_name
            return second_name

    def get_last_name(self, obj):
        if obj.relation_to_user.privacy:
            return _("user")
        else:
            last_name = obj.relation_to_user.last_name
            return last_name

    def get_last_activity(self, obj):
        last_activity = obj.relation_to_user.last_activity
        return last_activity

    def get_language(self, obj):
        language = obj.relation_to_user.language
        return language

    def get_***(self, obj):
        privacy = obj.relation_to_user.privacy
        return privacy

    def get_medical_information(self, obj):
        medical_information = obj.relation_to_user.medical_information
        return medical_information

    def get_***(self, obj):
        try:
            instance = UserPhoto.objects.get(relation_to_user=obj.relation_to_user)
        except UserPhoto.DoesNotExist as e:
            return {'ERROR': str(e)}
        photo_link = AuthorPhotoSerializer(instance=instance, read_only=True)
        return photo_link.data['image']['medium_square_crop']

    def get_user_id(self, obj):
        user_id = obj.relation_to_user.id
        return user_id

    def create(self, validated_data):
        instance = ***.objects.create(**validated_data)
        return instance

    def update(self, instance, validated_data):
        instance.relation_to_user.*** = self.context.get('***', instance.relation_to_user.**)
        instance.relation_to_user.last_name = self.context.get('last_name', instance.relation_to_user.last_name)
        instance.relation_to_user.second_name = self.context.get('second_name', instance.relation_to_user.second_name)
        instance.relation_to_user.language = self.context.get('language', instance.relation_to_user.language)
        instance.relation_to_user.*** = self.context.get('***', instance.relation_to_user.***)
        instance.relation_to_user.*** = self.context.get('***', instance.relation_to_user.***)
        instance.relation_to_user.save()

        # instance.relation_to_user.last_activity = self.context.get('last_activity')   wtf?

        instance.balance = validated_data.get('balance', instance.balance)
        instance.*** = validated_data.get('***', instance.***)
        instance.birthday = validated_data.get('birthday', instance.birthday)
        instance.telephone_number = validated_data.get('telephone_number', instance.telephone_number)
        instance.address = validated_data.get('address', instance.address)
        instance.*** = validated_data.get('***', instance.***)
        instance.*** = validated_data.get('***', instance.***)
        instance.*** = validated_data.get('***', instance.***)
        instance.*** = validated_data.get('**', instance.***)
        instance.*** = validated_data.get('***', instance.***)
        instance.*** = validated_data.get('***', instance.***)
        instance.*** = validated_data.get('***', instance.***)
        instance.*** = validated_data.get('***', instance.***)
        instance.*** = validated_data.get('***', instance.***)
        instance.*** = validated_data.get('***', instance.***)
        instance.*** = validated_data.get('**', instance.***)
        instance.height = validated_data.get('height', instance.height)
        instance.weight = validated_data.get('weight', instance.weight)
        instance.*** = validated_data.get('***', instance.***)
        instance.*** = validated_data.get('***', instance.***)
        instance.*** = validated_data.get('***', instance.***)
        instance.*** = validated_data.get('***', instance.***)
        instance.city = validated_data.get('city', instance.city)
        instance.street = validated_data.get('street', instance.street)
        instance.country = validated_data.get('country', instance.country)
        for i in validated_data.get('relation_to_images', instance.relation_to_images.all()):
            instance.relation_to_images.add(i)
        instance.save()
        return instance


class UserFilesSerializer(serializers.ModelSerializer):
    file = serializers.FileField(max_length=None, use_url=True)

    def create(self, validated_data):
        analyzes_ins = self.context['analyzes_instance']
        a = UserFiles.objects.create(**validated_data)
        # analyzes_ins.relation_to_files.add(a)
        return a

    class Meta:
        model = UserFiles
        fields = ('id', 'file', 'date_of_add', 'name_file', '***')
        # exclude = ('***',)

    def validate_file(self, value):
        in_dict = value.__dict__
        try:

            image = Image.open(value)
            import uuid
            from middleware.get_current_user import get_current_user

            name = in_dict.get('_name')
            valid_extensions = ['bmp', 'tiff', 'gif', 'jpg', 'png', 'jpeg', 'ico', 'im', 'msp', 'pcx', 'ppm', 'sgi']
            ext = name.split('.')[-1]
            if not ext.lower() in valid_extensions:
                raise ValidationError(_('Unsupported file extension.'))

            if in_dict.get('_size', 0) > 300000:
                maxsize = (1280, 1280)
                image.thumbnail(maxsize, Image.ANTIALIAS)
                filename = os.path.join('media', 'user_files', get_current_user().email, str(uuid.uuid4()) + '.jpg')
                os.makedirs(os.path.dirname(filename), exist_ok=True)
                image.save(filename, 'JPEG', quality=75)
                return filename
            return value

        except IOError:
            size = in_dict.get('_size')
            if size > 1073741824:
                raise ValidationError(_('Unsupported file size. file must be less than 1Gb'))
            file_ext = str(value).split('.')
            ext = file_ext[-1]
            valid_extensions = ['pdf', 'doc', 'docx', 'jpg', 'png', 'xlsx', 'xls', 'avi', 'webm', 'mkv', 'flv', 'mp4',
                                'sxw ', 'txt', 'tar', 'iso', '7z', 's7z', 'rar', 'zip']
            if not ext.lower() in valid_extensions:
                raise ValidationError(_('Unsupported file extension.'))
            return value


class FileListSerializer(serializers.Serializer):
    file = serializers.FileField(max_length=100000, allow_empty_file=False, use_url=False)

    def validate_file(self, value):
        in_dict = value.__dict__
        try:

            image = Image.open(value)
            import uuid
            from middleware.get_current_user import get_current_user

            name = in_dict.get('_name')
            valid_extensions = ['bmp', 'tiff', 'gif', 'jpg', 'png', 'jpeg', 'ico', 'im', 'msp', 'pcx', 'ppm', 'sgi']
            ext = name.split('.')[-1]
            if not ext.lower() in valid_extensions:
                raise ValidationError(_('Unsupported file extension.'))

            if in_dict.get('_size', 0) > 300000:
                maxsize = (1280, 1280)
                image.thumbnail(maxsize, Image.ANTIALIAS)
                filename = os.path.join('media', 'user_files', get_current_user().email, str(uuid.uuid4()) + '.jpg')
                os.makedirs(os.path.dirname(filename), exist_ok=True)
                image.save(filename, 'JPEG', quality=75)
                return filename
            return value

        except IOError:
            size = in_dict.get('_size')
            if size > 1073741824:
                raise ValidationError(_('Unsupported file size. file must be less than 1Gb'))
            file_ext = str(value).split('.')
            ext = file_ext[-1]
            valid_extensions = ['pdf', 'doc', 'docx', 'jpg', 'png', 'xlsx', 'xls', 'avi', 'webm', 'mkv', 'flv', 'mp4',
                                'sxw ', 'txt', 'tar', 'iso', '7z', 's7z', 'rar', 'zip']
            if not ext.lower() in valid_extensions:
                raise ValidationError(_('Unsupported file extension.'))
            return value


from interrelation.models import ***


class FileObjectRelatedField(serializers.RelatedField):
    def to_representation(self, value):
        if isinstance(value, Task):
            return '***' + (value.id)
        elif isinstance(value, UserAnalyzes):
            return '***' + (value.id)
        else:
            raise Exception('Unexpected type of tagged object')
        return serializer.data


class FilesObjectRelatedField(serializers.RelatedField):
    def to_representation(self, value):
        if isinstance(value, ***):
            return '***: ' + value.id
        elif isinstance(value, ***):
            return '***: ' + value.id
        raise Exception('Unexpected type of tagged object')


class RequestFilesSerializer(serializers.ModelSerializer):
    file = serializers.FileField(max_length=100000, allow_empty_file=False, use_url=True)
    # name_file = serializers.SerializerMethodField()
    name_file = serializers.CharField()
    model_name = serializers.CharField(required=False, allow_blank=True)

    class Meta:
        model = UserFiles
        fields = ('id', 'date_of_add', 'name_file', 'relation_to_user', 'file', 'file_url', 'content_type', 'object_id',
                  'model_name')
        read_only_fields = ('relation_to_user', 'file_url')

    def create(self, validated_data):
        name_file = validated_data.get('name_file', None)
        object_id = validated_data.get('object_id', None)
        relation_to_user = validated_data.get('relation_to_user', None)
        file = validated_data.get('file', None)
        model_name = validated_data.get('model_name', None)
        model_instance = None
        if model_name == '***':
            model_instance = ***.objects.get(id=object_id)
        elif model_name == '***':
            model_instance = ****k.objects.get(id=object_id)
        content_type = ContentType.objects.get_for_model(model_instance)

        request_file_obj = UserFiles.objects.create(content_type=content_type, object_id=object_id,
                                                    relation_to_user=relation_to_user, file=file, name_file=name_file)
        return request_file_obj

    def to_representation(self, obj):
        # get the original representation
        remove = super(RequestFilesSerializer, self).to_representation(obj)
        # remove fields
        remove.pop('file')
        return remove

    def validate_file(self, validated_data):
        in_dict = validated_data.__dict__
        file_obj = validated_data
        from middleware.get_current_user import get_current_user
        try:
            image = Image.open(file_obj)
            import uuid

            name = in_dict.get('_name')
            valid_extensions = ['bmp', 'tiff', 'gif', 'jpg', 'png', 'jpeg', 'ico', 'im', 'msp', 'pcx', 'ppm', 'sgi']
            ext = name.split('.')[-1]
            if not ext.lower() in valid_extensions:
                raise ValidationError(_('Unsupported file extension.'))

            if in_dict.get('_size', 0) > 300000:
                maxsize = (1280, 1280)
                image.thumbnail(maxsize, Image.ANTIALIAS)
                filename = os.path.join('media', 'user_files', get_current_user().email, str(uuid.uuid4()) + '.jpg')
                os.makedirs(os.path.dirname(filename), exist_ok=True)
                image.save(filename, 'JPEG', quality=75)
                # file = UserFiles.objects.create(file=file_obj, name_file=name,
                #                                 relation_to_user=self.context.get('user'))
        except IOError:
            name_file = in_dict.get('_name')
            content_type = in_dict.get('content_type')
            size = in_dict.get('_size')
            if size > 1073741824:
                raise ValidationError(_('Unsupported file size. file must be less than 1Gb'))
            file_ext = str(file_obj).split('.')
            ext = file_ext[-1]
            valid_extensions = ['pdf', 'doc', 'docx', 'jpg', 'png', 'xlsx', 'xls', 'avi', 'webm', 'mkv', 'flv',
                                'mp4',
                                'sxw ', 'txt', 'tar', 'iso', '7z', 's7z', 'rar', 'zip']
            if not ext.lower() in valid_extensions:
                raise ValidationError(_('Unsupported file extension.'))
            if name_file:
                filename = os.path.join('media', 'user_files', get_current_user().email, name_file)
                os.makedirs(os.path.dirname(filename), exist_ok=True)
            else:
                raise ValidationError(_('No valid name of current file'))
        return file_obj


class User****Serializer(serializers.ModelSerializer):
    # relation_to_user_files = RequestFilesSerializer(many=True, read_only=True)
    everything_data = JSONSerializerField()
    manual_date = serializers.DateTimeField(required=False)
    files = RequestFilesSerializer(many=True, required=False, allow_null=True)
    ****_related = serializers.SerializerMethodField(required=False, read_only=True, allow_null=True)

    class Meta:
        model = UserAnalyzes
        fields = ('id', 'date_of_****', 'manual_date', 'title_****', 'organ_title', 'everything_data', 'files',
                  '***_related', 'updated', 'created')
        read_only_fields = ('id', 'files', '***_related')

    def get_***_related(self, instance):
        import interrelation

        *** = instance.***_analyzes.all()
        serializer = interrelation.serializers.***Serializer(instance=***, many=True)
        
        return serializer.data

        

class DocCategoriesSerializer(serializers.ModelSerializer):
    current_category = serializers.SerializerMethodField()

    class Meta:
        model = DocCategories
        fields = ('id', 'current_category')

    def get_current_category(self, obj):
        current_language = translation.get_language()
        value_to_return = obj.name.get(current_language)
        return {current_language: value_to_return}


class ****Serializer(serializers.ModelSerializer):
    **** = ***Serializer(many=True, read_only=True, required=False)
    relation_to_images = ImagesEveryoneSerializer(many=True, read_only=True, required=False)
  
    first_name = serializers.SerializerMethodField()
    second_name = serializers.SerializerMethodField()
    last_name = serializers.SerializerMethodField()
    language = serializers.SerializerMethodField()
    ***_photo = serializers.SerializerMethodField(required=False)
    *** = serializers.SerializerMethodField()
    user_id = serializers.SerializerMethodField()
    last_activity = serializers.SerializerMethodField()
    **** = serializers.SerializerMethodField()
    type = serializers.SerializerMethodField(required=False, allow_null=True)
    url = serializers.SerializerMethodField(required=False, allow_null=True)

   

    class Meta:
        model = ***
        fields = ('id', 'user_id', '***', '***', 'first_name', 'second_name', 'last_name', 'status',
                  'language', 'country', 'phone_number', 'place_of_work', 'city_of_work', 'description', '***',
                  '***', 'cost', '***', '****', '***', 'additional_files',
                  'relation_to_images', 'relation_to_user', '***',
                  'last_activity', 'language', 'type', 'url')
        read_only_fields = ('id', 'last_activity', 'type', 'url')

  
    def get_type(self, obj):
        if self.context.get('user_initial_path'):
            if 'search' in self.context.get('user_initial_path'):
                # print(self.__class__.__name__)
                return ('search_doc_profile')

    def get_url(self, obj):
        ***_id = obj.id
        url = 'api***/***/%s' % ***_id
        return url

    def get_***_***(self, obj):
        user = User.objects.filter(id=obj.relation_to_user.id)

    def get_first_name(self, obj):
        first_name = obj.relation_to_user.first_name
        return first_name

    def get_second_name(self, obj):
        second_name = obj.relation_to_user.second_name
        return second_name

    def get_last_name(self, obj):
        last_name = obj.relation_to_user.last_name
        return last_name

    def get_last_activity(self, obj):
        last_activity = obj.relation_to_user.last_activity
        return last_activity

    def get_language(self, obj):
        language = obj.relation_to_user.language
        return language

    def get_***_photo(self, obj):
        try:
            instance = UserPhoto.objects.get(relation_to_user=obj.relation_to_user)
        except UserPhoto.DoesNotExist as e:
            return {'error': str(e)}
        photo_link = UserPhotoSerializer(instance=instance, read_only=True)
        return photo_link.data['image']

    def get_***(self, obj):
        ***_ins = obj.relation_to_user.****
        return ***

    def get_ratings(self, obj):
        ***_ins = obj.relation_to_user.****
        return ***_ins

    def get_user_id(self, obj):
        ***_id = obj.relation_to_user.id
        return ***_id

    def create(self, validated_data):
        instance = ***.objects.create(**validated_data)
        list_of_*** = instance.relation****.values_list('id', flat=True)
        if validated_data.get('***'):
            relations_to_*** = map(int, validated_data.get('***').split(','))
            for *** in relations_to_***:
                if category not in list_of_***:
                    instance.relation_to***_***.add(***)
        else:
            instance.save()
        return instance

    def update(self, instance, validated_data):
        asds = (self.context.get('***'))
        instance.relation_to_****.clear()  *
        # print(validated_data.get(***'))
        list_cats = self.context.get(***').split(',')
        if self.context.get('***'):
            relations_to_*** = list(map(lambda x: int(x.strip()), list_cats))
        else:
            relations_to_*** = []
        list_of_*** = instance.***_***s.values_list('id', flat=True)
        if self.context.get('***'):
            for *** in ***:
                if ** not in **y:
                   
                    instance.relation_to_***.add(**)
                   
        else:
            instance.save()
       
        instance.relation_to_user.first_name = self.context.get('first_name', instance.relation_to_user.first_name)
        instance.relation_to_user.last_name = self.context.get('last_name', instance.relation_to_user.last_name)
        instance.relation_to_user.second_name = self.context.get('second_name', instance.relation_to_user.second_name)
        instance.relation_to_user.language = self.context.get('language', instance.relation_to_user.language)
        instance.relation_to_user.save()

        instance.language = validated_data.get('language', instance.language)
        instance.country = validated_data.get('country', instance.country)
        instance.phone_number = validated_data.get('phone_number', instance.phone_number)
        instance.*** = validated_data.get('***', instance.**)
        instance.** = validated_data.get('**', instance.**)
        instance.description = validated_data.get('description', instance.description)
        instance.** = validated_data.get('**', instance.**)
        instance.** = validated_data.get('**', instance.***)
        instance.*** = validated_data.get('***', instance.***)
        instance.*** = validated_data.get('***', instance.***)
        instance.*** = validated_data.get('***', instance.***)
        instance.*** = validated_data.get('***', instance.***)
        instance.additional_files = validated_data.get('additional_files', instance.additional_files)
        
        instance.save()
        return instance


class Base64ImageField(VersatileImageFieldSerializer):
    """
    A Django REST framework field for handling image-uploads through raw post data.
    It uses base64 for encoding and decoding the contents of the file.

    Heavily based on
    https://github.com/tomchristie/django-rest-framework/pull/1268

    Updated for Django REST framework 3.
    """

    def to_internal_value(self, data):
        from django.core.files.base import ContentFile
        import base64
        import six
        import uuid

        # Check if this is a base64 string
        if isinstance(data, six.string_types):
            # Check if the base64 string is in the "data:" format
            if 'data:' in data and ';base64,' in data:
                # Break out the header from the base64 content
                header, data = data.split(';base64,')

            # Try to decode the file. Return validation error if it fails.
            try:
                decoded_file = base64.b64decode(data)
            except TypeError:
                self.fail('invalid_image')

            # Generate file name:
            file_name = str(uuid.uuid4())[:12]  # 12 characters are more than enough.
            # Get the file name extension:
            file_extension = self.get_file_extension(file_name, decoded_file)

            complete_file_name = "%s.%s" % (file_name, file_extension,)

            data = ContentFile(decoded_file, name=complete_file_name)

        return super(Base64ImageField, self).to_internal_value(data)

    def get_file_extension(self, file_name, decoded_file):
        import imghdr

        extension = imghdr.what(file_name, decoded_file)
        extension = "jpg" if extension == "jpeg" else extension

        return extension


class UserPhotoSerializer(serializers.ModelSerializer):
    image = Base64ImageField(
        sizes=[
            ('full_size', 'url'),
            ('thumbnail', 'thumbnail__100x100'),
            ('medium_square_crop', 'crop__400x400'),
            ('small_square_crop', 'crop__50x50'),
            ('medium_crop', 'crop__240x240')
        ]
    )

    class Meta:
        model = UserPhoto
        fields = ('id', 'name', 'image')
        read_only_fields = ('id',)


class ChangePasswordSerializer(serializers.Serializer):
    """
    Serializer for password change endpoint.
    """
    old_password = serializers.CharField(required=True)
    new_password = serializers.CharField(required=True)


class UserProfileByDoctorSerializer(serializers.ModelSerializer):
  
    full_user_profile = serializers.SerializerMethodField()
    full_profile = serializers.SerializerMethodField()

    user_photo = serializers.SerializerMethodField(required=False)
   ***_counts = serializers.SerializerMethodField()
    user_*** = serializers.SerializerMethodField()

    def __init__(self, *args, **kwargs):
        remove_fields = kwargs.pop('remove_fields', None)
        super(UserProfileByDoctorSerializer, self).__init__(*args, **kwargs)
        if remove_fields:
            for field in remove_fields:
                self.fields.pop(field)

    class Meta:
        model = get_user_model()
        fields = ('id', 'full_user_profile', 'email', 'first_name', 'second_name', 'last_name',
                  'user_photo', '***_counts', 'user_rating', 'full_profile')
        read_only_fields = ('user_photo', '***_counts', 'user_rating', 'full_user_profile')

    def get_user_photo(self, obj):
        try:
            instance = UserPhoto.objects.get(relation_to_user__username=obj.username)
        except UserPhoto.DoesNotExist as e:
            return {'ERROR': str(e)}
        photo_link = UserPhotoSerializer(instance=instance, read_only=True)
        return photo_link.data['image']

    def get_***_counts(self, obj):
        from interrelation.models import***
        status = ['Open']
        
        *** = ***.objects.filter(relation_to_user=(obj.id)).exclude(status__in=status).count()
        return ***

    def get_user_rating(self, obj):
        from interrelation.models import Ratings
        from interrelation.serializers import RatingsSerializer
        try:
            rating = Ratings.objects.get(relation_to_user=obj.id)
        except Ratings.DoesNotExist:
            return _("user is not rated yet.")
        serializer = RatingsSerializer(instance=rating)
        return serializer.data

    def get_full_profile(self, obj):
        ***_information_fields = ['**', '**', '***', '***', '***',
                                      '***', '***', '***', '***',
                                      '***', '***', '***', '***',
                                      '***', '**', '***']

        full_privacy_fields = ['***', '***', '***', 'telephone_number', 'address', 'street', 'country',
                               'relation_to_images', 'first_name', 'second_name', 'last_name', 'user_photo']
        if obj.privacy:
            bio_details = UserBioDetails.objects.get(relation_to_user=obj.id)
            serializer = UserBioDetailsSerializer(instance=bio_details,
                                                  remove_fields=full_privacy_fields + ***_fields)

        elif not obj.***:
            bio_details = UserBioDetails.objects.get(relation_to_user=obj.id)
            serializer = UserBioDetailsSerializer(instance=bio_details, remove_fields=***_fields)
        else:
            bio_details = UserBioDetails.objects.get(relation_to_user=obj.id)
            serializer = UserBioDetailsSerializer(instance=bio_details)
        return serializer.data


class LawAgreementSerializer(serializers.ModelSerializer):
    class Meta:
        model = LawAgreement
        fields = ('id', 'relation_to_text', 'relation_to_user', 'updated', 'timestamp', 'user_agreement', 'user_adult',
                  "privacy_agreement", "terms_agreement",)
        read_only_fields = ('relation_to_user', 'updated', 'timestamp',)




class TextAgrementSerializer(serializers.ModelSerializer):
    class Meta:
        model = TextAgrement
        fields = ("id", "title", "text_agreement",)


class SmsCodeSerializer(serializers.ModelSerializer):
    delivered = serializers.SerializerMethodField(required=False, read_only=True)

    class Meta:
        model = SmsCode
        fields = (
        'user', 'phone_number', 'code', 'confirmed', 'created_at', 'confirmed_at', 'batch_id', 'delivered', 'active')
        read_only_fields = ('confirmed', 'created_at', 'confirmed_at', 'batch_id', 'delivered', 'active')

    def get_delivered(self, obj):
        batch_id = obj.batch_id
        from .sms_clx import is_delivered
        return is_delivered(batch_id)


class SmsPasswordResetCodeSerializer(serializers.ModelSerializer):
    delivered = serializers.SerializerMethodField(required=False, read_only=True)

    class Meta:
        model = SmsCode
        fields = (
        'user', 'phone_number', 'code', 'confirmed', 'created_at', 'confirmed_at', 'batch_id', 'delivered', 'active')
        read_only_fields = ('confirmed', 'created_at', 'confirmed_at', 'batch_id', 'delivered', 'active')

    def get_delivered(self, obj):
        batch_id = obj.batch_id
        from .sms_clx import is_delivered
        if obj.batch_id:
            status = is_delivered(batch_id)
            if status.status_code == '200':
                return True
            else:
                return False

    def to_representation(self, instance):
        remove = super(SmsPasswordResetCodeSerializer, self).to_representation(instance)
        # remove fields
        remove.pop('code')
        remove.pop('batch_id')
        remove.pop('delivered')
        return remove

