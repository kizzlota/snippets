from storages.backends.sftpstorage import SFTPStorage, SFTPStorageFile
from django.conf import settings
import paramiko
from paramiko.client import SSHClient
import os
import sys
from django.core.files.storage import Storage
from storages.utils import setting
import uuid
from rest_framework.authtoken.models import Token
from profiles.models import UserFiles
from profiles.serializers import UserFilesSerializer
import os
from rest_framework import status
import time
from celery.task import task


# PARAMIKO_HOST =***
# PARAMIKO_USER = ***
# PARAMIKO_PASSWORD = ***
# PARAMIKO_PORT = ***
#


@task(default_retry_delay=3 * 20, max_retries=3)
def celery_loader(parameters):
    transport = paramiko.Transport((***, ***))
    if not transport.is_active():
        try:
            transport.connect(***, ***)
           
        except:
            print(sys.exc_info())
        ftp_server = paramiko.SFTPClient.from_transport(transport)

        files_locate = parameters.get('files_locate')
        filenm = str(parameters.get('filenm'))
        user = str(parameters.get('user'))
        file_id = str(parameters.get('file_id'))
        user_id = str(parameters.get('user_id'))
        url = str(parameters.get('url'))
        size_file = str(parameters.get('size_file'))
        name_file = str(parameters.get('name_file'))

        ftp_server.chdir(user)
        ftp_server.put(files_locate, filenm)

        loca_path = os.path.dirname(os.path.dirname(__file__))
        local_user_files_path = os.path.join(loca_path, 'media', 'user_files', user)

        try:
            file_instnace = ***.objects.get(id=file_id, ***=***)
        except UserFiles.DoesNotExist as e:
            raise LookupError('File not found')
        serializer = UserFilesSerializer(instance=file_instnace)
        if serializer:
            instnanse = serializer.instance
            instnanse.file_path = ***

            file_name_local = os.path.join(local_user_files_path, name_file)
            os.remove(file_name_local)
            instnanse.save()


class StorageUserFiles(Storage):
    def __init__(self, file_url=None, files_list=[], user_identificator=None, known_host_file=None, filename=None,
                 connecion_params=None):
        self.file_url = file_url
        self.files_list = files_list
        self.user_identificator = self._username(user_identificator)
        self.parameters = connecion_params
        self.filename = filename
        self.connection = self.connect()

    def _username(self, username):
        if not isinstance(username, str):
            username = str(username)
        if '@' in username:
            username = username.replace('@', '_')
        return username

    def connect(self, parameters=None):

        transport = paramiko.Transport((***, ***))
        
        if not transport.is_active():
            try:
                transport.connect(username='***', password='***')
               
            except:
                print(sys.exc_info())
            ftp_client = paramiko.SFTPClient.from_transport(transport)
            return ftp_client
        return 'Already connected'

    def get_info_connection(self):
        client = paramiko.SSHClient()
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        client.connect(hostname=***', username='***', password='***', port=***)
        stdin, stdout, stderr = client.exec_command('***')
        client.close()

    def filename_generator(self, filename=None):
        data = []
        if filename:
            if not '.' in filename:
                raise ValueError('file must be provided with file extension')
            else:
                filename = filename
        elif not filename:
            filename = self.filename
        elif not filename:
            ext = filename.split('.')[-1]
            if not filename:
                filename = "%s.%s" % (uuid.uuid4(), ext)
        get_user = self.user_identificator
       
        data.append(get_user)
        data.append(str(filename))

        filename = '_'.join(data)
        return filename

    def user_directory_checker(self, user):
       
        if not user:
            user = self.user_identificator
        if user:
            sftp_server = self.connection
            remote_path = '/home/kizzlota/' + str(user) + '/'
           
            check = None
            try:
                check = sftp_server.stat(remote_path)
                return True
            except FileNotFoundError:
                return False
        else:
            raise ValueError('no user instance present')

    def user_directory_name_generator(self, user=None):
        if not user:
            user = self.user_identificator
        user_dir = self.user_directory_checker(user=user)
        if not user_dir:
           
            sftr_server = self.connection
            folder = None
            folder = sftr_server.mkdir(str(self.user_identificator))
            return self.user_identificator
        else:
            # TODO check this error than directory is exists
            raise LookupError('disrectory alredy exists')

    def put_file(self, file=None, filename=None):
        url = str(self.file_url)
        fl = url.split('/')
        filename = fl[-1]
        if not file:
            file = str(self.file_url)
        if isinstance(file, str):
            fl = file.split('/')
            if isinstance(fl, list):
                file = fl[-1]

        user = str(self.user_identificator)
        if '@' in user:
            user = user.replace('@', '_')
        if filename:
            filenm = self.filename_generator(filename)
        else:
            filenm = self.filename_generator()
        server_directroty = str(user)
        is_user_dir_exist = self.user_directory_checker(str(self.user_identificator))
        if not is_user_dir_exist:
            server_directroty = self.user_directory_name_generator(user=user)
        sftp_server = self.connect()
        sftp_server.chdir(str(self.user_identificator))
        file_location = os.path.join(settings.BASE_DIR, 'media', str(user), file)
        filesss = os.path.join(settings.BASE_DIR, 'media', 'user_files', str(user), str(file))
        sftp_server.chdir(***)
        sftp_server.chdir(str(self.user_identificator))
        sftp_server.put(filesss, str(filenm))

        return {'OK': 'File successfullty uploaded'}

    def ulpoad_list_files(self, files=None):
        sftp_server = self.connection
        if sftp_server:
            if files:
                if not isinstance(files, list):
                    raise ValueError('files must be in list')
            else:
                files = self.files_list
                if not isinstance(files, list):
                    raise ValueError('files must be in list')
            user = str(self.user_identificator)
            if '@' in user:
                user = user.replace('@', '_')
            server_directroty = str(user)
            is_user_dir_exist = self.user_directory_checker(self.user_identificator)
            if not is_user_dir_exist:
                server_directroty = self.user_directory_name_generator(user=user)
            sftp_server.chdir(str(self.user_identificator))

            loca_path = os.path.dirname(os.path.dirname(__file__))
            for file in files:

                file_id = file.get('id')
                user_id = file.get('relation_to_user')
                url = file.get('file_url')
                size_file = file.get('size_file')
                name_file = file.get('name_file')

                fl = url.split('/')
                filename = fl[-1]
                if filename:
                    filenm = self.filename_generator(filename)
                else:
                    filenm = self.filename_generator()
                files_locate_in = os.path.join(loca_path, 'media', 'user_files', str(user), str(filename))
                parameters = {'files_locate': files_locate_in, 'filenm': filenm, 'user': self.user_identificator,
                              'file_id': file_id, 'user_id': user_id, 'url': url, 'size_file': size_file,
                              'name_file': name_file}

                celery_loader.delay(parameters)
                continue
              
        sftp_server.close()
        return True

    def delete_remote_files(self, list_ids=None):
        sftp_server = self.connection
        files_query = UserFiles.objects.filter(id__in=list_ids)
        if not files_query:
            raise ValueError({'ERROR': 'No files to delete'})
        sftp_server.chdir(str(self.user_identificator))
        for file in files_query:
            if file.file_path:
                string_split = file.file_path.split('/')
                filename = string_split[-1]
                try:
                    sftp_server.remove(filename)
                except FileNotFoundError as e:
                    return {"ERROR": str(e)}
        sftp_server.close()
        return "ok"

    def check_general_capacity_of_files(self):
      
        remote_sftp_server = self.connection
        user = self.user_identificator

        is_user_dir_exist = self.user_directory_checker(str(self.user_identificator))
        if not is_user_dir_exist:
            server_directroty = self.user_directory_name_generator(user=user)

        try:
            list_dir = remote_sftp_server.listdir_attr(user)
        except FileNotFoundError as e:
            raise FileNotFoundError(str(e))
        all_files_in_dir_size = 0
        for item in list_dir:
            # print(item.filename)
            # print(item.st_size)
            all_files_in_dir_size += item.st_size
            # print(list_dir)
        return {'content_capacity': all_files_in_dir_size}

    def get_filenames_before_upload(self):
        user = self.user_identificator
        sftp_server = self.connection
        is_user_dir_exist = self.user_directory_checker(str(self.user_identificator))
        if not is_user_dir_exist:
            server_directroty = self.user_directory_name_generator(user=user)
        try:
            list_dir = sftp_server.listdir_attr(user)
        except FileNotFoundError as e:
            raise FileNotFoundError(str(e))
        files_information = {}
        file_details = []
        all_files_in_dir_size = 0
        for file in list_dir:
            file_details.append({'filename': file.filename, 'file_size': file.st_size})
            all_files_in_dir_size += file.st_size
        files_information['content_capacity'] = all_files_in_dir_size
        files_information['files'] = file_details
        return files_information

    def upload_proccess_with_file_to_exclude(self, request=None):
        data_files = request.data.get('data_files')
        # TODO check if this method works in real conditions with newly iuploaded files
        sftp_server = self.connection
        user = self.user_identificator

        filenames_for_exclude = self.get_filenames_before_upload()

        loca_path = os.path.dirname(os.path.dirname(__file__))
        files_locate_in = os.path.join(loca_path, 'media', 'user_files', str(user))
        os.chdir(files_locate_in)
        user_loca_files = os.listdir()

        is_user_dir_exist = self.user_directory_checker(str(self.user_identificator))
        files_information = None
        if not is_user_dir_exist:
            server_directroty = self.user_directory_name_generator(user=user)
        try:
            list_dir = sftp_server.listdir_attr(user)
            # print(list_dir)
        except FileNotFoundError as e:
            raise FileNotFoundError(str(e))
        if filenames_for_exclude:
            # file markers that need to be excluded
            capacicty_of_existing_files = int(filenames_for_exclude.get('content_capacity'))
            files_exl = filenames_for_exclude.get('files')
            names = [name.get('filename') for name in files_exl]

            # general information of validated files
        files_information = {}
        file_details = []
        all_files_in_dir_size = 0
        for file in list_dir:
            for request_file in data_files:
                request_file_filename = request_file.get('filename')
                req_file_size = request_file.get('size')
                final_name_to_compare = str(self.user_identificator) + '_' + request_file_filename
                if file.filename == final_name_to_compare:
                    full_size = req_file_size
                    current_size = file.st_size
                    percentage = int(current_size) * 100 / int(full_size)
                    file_details.append({
                        'filename': file.filename,
                        'file_size': file.st_size,
                        'initial_file_size': req_file_size,
                        'percentage': percentage})
            all_files_in_dir_size += file.st_size
        files_information['content_capacity'] = all_files_in_dir_size
        files_information['files'] = file_details
        files_information['total_capacity'] = int(capacicty_of_existing_files) + int(all_files_in_dir_size)
        new_names = files_information.get('files')
        # print(new_names)
        for nn in new_names:
            # print(nn)
            filename = nn.get('filename')
            if filename in names:
                for number, item in enumerate(new_names):
                    new_names.pop(number)
        files_information['files'] = new_names
        user_loca_files = os.listdir()
        if not user_loca_files:
            files_information['all_files_uploaded'] = True
        else:
            files_information['all_files_uploaded'] = False
        return files_information

