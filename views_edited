# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render
from django.db.models import Q
from bidirectional.models import *
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated, AllowAny
from interrelation.models import Tr, Jobs, WorkFlow
from profiles.models import UserBioDetails, User, DProfile
from bidirectional.serializers import *
from interrelation.serializers_logic.workflow import WorkFlowSerializer
# from interrelation.serializers import WorkFlowSerializer
from rest_framework.response import Response
from rest_framework import status
import datetime
import json
import ast
from django.db.models import Q
from .events import EventManager
from django.utils.translation import ugettext_lazy as _
import yaml


# Create your views here.


class EventViewSet(viewsets.ViewSet):
    permission_classes = (IsAuthenticated,)

    def list(self, request):
        """
        ---
        request_serializer: EventsSerializer
        response_serializer: EventsSerializer
        """
        event_obj = Events.objects.filter(author=request.user.id).values()
        if event_obj:
            event_flag = None
            event_result_dict = {}
            for event in event_obj:
                if event_flag != event['type']:
                    event_flag = event['type']
                    event_ids = []
                    event_filter = event_obj.filter(type=event_flag)
                    for eve_obj in event_filter:
                        event_ids.append(eve_obj['event_mark'])
                        event_result_dict[eve_obj['type']] = {'count': event_filter.count(), 'ids': event_ids}
            return Response(event_result_dict, status=status.HTTP_200_OK)
        else:
            return Response({'ERROR': _('Events are empty')}, status=status.HTTP_204_NO_CONTENT)

    def update(self, request, id):
        """
        ---
        request_serializer: EventsSerializer
        response_serializer: EventsSerializer
        """
        try:
            event_object = Events.objects.get(id=id)
        except Events.DoesNotExist as e:
            return Response({'ERROR': str(e)}, status=status.HTTP_400_BAD_REQUEST)
        event_object.read_at = datetime.datetime.now()
        event_object.save()
        return Response(status=status.HTTP_200_OK)

    def delete(self, request, id):
        try:
            event = Events.objects.get(Q(id=id), (Q(author=request.user.id)| Q(event_participants__icontains=request.user)))
        except Events.DoesNotExist as e:
            return Response({'ERROR': str(e)}, status=status.HTTP_400_BAD_REQUEST)
        event.delete()
        return Response(status=status.HTTP_200_OK)

    def retrieve(self, request, parameter):
        """
        ---
        request_serializer: EventsSerializer
        response_serializer: EventsSerializer
        """
        events_obj = Events.objects.filter(event_participants__icontains=request.user, read_at__isnull=True)
        if events_obj:

            if str(parameter) == 'notificationEvents':
                events_not_read = [event for event in events_obj if
                                   event.read_at is None and str(event.type).lower() == 'message']
                if events_not_read:
                    for event in events_not_read:
                        event.read_at = datetime.datetime.now()
                        event.save()
            elif str(parameter) == 'notMessages':
                events_not_read = [event for event in events_obj if
                                   event.read_at is None and not str(event.type).lower() == 'message']
                if events_not_read:
                    for event in events_not_read:
                        event.read_at = datetime.datetime.now()
                        event.save()
            return Response({'OK': 'all events are marked as read'}, status=status.HTTP_200_OK)
        return Response({"ERROR": _("no events to mark as read")})



        # event_not_read = [event for event in events_obj if event.read_at is None]
        #
        # if event_not_read:
        #     for event in event_not_read:
        #         if parameter == 'notification-events':
        #             if str(event.type).lower() == 'message':
        #                 pass
        #         elif parameter == '' or parameter is None:
        #             event.read_at = datetime.datetime.now()
        #             event.save()
        #         else:
        #             event.read_at = datetime.datetime.now()
        #             event.save()
        #
        #     return Response({'OK': 'all events are marked as read'}, status=status.HTTP_200_OK)


class EventManagementViewSet(viewsets.ViewSet):
    permission_classes = (IsAuthenticated,)

    def list(self, request):
        """
        ---
        request_serializer: EventsSerializer
        response_serializer: EventsSerializer
        """
        events = Events.objects.filter(author=request.user.id, read_at__isnull=True)
        events_rec = Events.objects.filter(
            Q(event_participants__contains="{0}".format(request.user.email)), Q(read_at__isnull=True))
        serializer = EventsSerializer(events_rec, many=True)
        if serializer:
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response({'ERROR': _('there is no unread events')}, status=status.HTTP_400_BAD_REQUEST)

    def retrieve(self, request):
        """
        ---
        request_serializer: EventsSerializer
        response_serializer: EventsSerializer
        """
        events = Events.objects.filter(author=request.user)
        if events:
            serializer = EventsSerializer(events, many=True)
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response({'ERROR': _('Events are empty')}, status=status.HTTP_204_NO_CONTENT)


class MessagesViewSet(viewsets.ViewSet):
    permission_classes = (IsAuthenticated,)

    def list(self, request, id):
        """
        ---
        request_serializer: MessageSerializer
        response_serializer: MessageSerializer
        """
        # print "'{0}'".format(request.user.username)
        messages_ins = Message.objects.filter(Q(relation_to_job=id) &
                                              Q(chat_participants__contains="{0}".format(request.user.email)))
        if messages_ins.exists():
            # messages_ins = Message.objects.filter(relation_to_job__in=treatsments)
            serializer = MessageSerializer(messages_ins, many=True)
            if serializer:
                return Response(serializer.data, status=status.HTTP_200_OK)

            return Response({"ERROR": _('messages are empty')}, status=status.HTTP_204_NO_CONTENT)
        return Response({"ERROR": _('you don`t have partitions or there are no chats')},
                        status=status.HTTP_400_BAD_REQUEST)

    def create_message(self, request, id):
        """
        ---
        request_serializer: MessageSerializer
        response_serializer: MessageSerializer
        """
        serializer = MessageSerializer(data=request.data)
        messages_ins = Message.objects.filter(Q(relation_to_job=id) &
                                              Q(chat_participants__contains="{0}".format(request.user.email)))

        if messages_ins.exists():
            # messages_ins = Message.objects.filter(relation_to_job__in=treatsments)
            chat_object = ast.literal_eval(serializer.initial_data[
                                               'chat_json'])  # You cannot call `.save()` after accessing `serializer.data`'serializer.validated_data' instead.
            for i in messages_ins:
                check_message_count = i.id
            message_instance = Message.objects.get(id=check_message_count)
            serializer_update = MessageSerializer(instance=message_instance, data=request.data)

            update_chat = message_instance.chat_json

            data_message = {'id': message_instance.message_count + 1, 'type': '', 'price': '',
                            'sender': request.user.id, 'msg_body': chat_object.get('msg_body', ''),
                            'parent_msg': '', 'sent_at': str(datetime.datetime.now()), 'read_at': '', 'active': 'true'}

            update_chat['list_of_messages'].append(data_message)
            if serializer_update.is_valid():
                serializer_update.save(chat_json=update_chat, message_count=message_instance.message_count + 1)

                if message_instance.chat_participants:
                    participants = yaml.load(message_instance.chat_participants)
                    participants = participants[0]
                    t_instance = message_instance.relation_to_job.relation_to_t

                    if not request.user.is_d:
                        user_data = participants.get('ds')
                        user_id = user_data[0].get('d_id')
                        recipient = User.objects.get(id=user_id)
                        url = '/d/t/{0}/'.format(t_instance.id)
                        string = '{text} {treat} {t2} {author}'.format(text=_('You have new message in consultation '),
                                                                       treat=t_instance.title,
                                                                       author=request.user.email,
                                                                       t2=_('from ')
                                                                       )
                    else:
                        d_data = participants.get('users')
                        d_id = d_data[0].get('user_id')
                        recipient = User.objects.get(id=d_id)
                        url = '/user/t/{0}/'.format(trt_instance.id,)
                        string = '{text} {treat} {t2} {author}'.format(text=_('You have new message in consultation '),
                                                                       treat=t_instance.title,
                                                                       author=request.user.email,
                                                                       t2=_('from '))
                    event = EventManager(type='message', action='create', event_mark=serializer_update.data.get('id'),
                                         author=request.user, recepient=[recipient], url=url, string=string)
                    event.event_create()

                return Response(serializer_update.data, status=status.HTTP_200_OK)
            return Response({"ERROR": _('messages are empty')}, status=status.HTTP_204_NO_CONTENT)
        return Response({"ERROR": _('you don`t have partitions or there are no chats')},
                        status=status.HTTP_400_BAD_REQUEST)



        #     if serializer.is_valid():
        #         serializer.save(chat_json=data_message, chat_participants='{0},{1}'.format(user_member, dr_member))
        #         return Response(serializer.data, status=status.HTTP_201_CREATED)
        #     return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        #
        # return Response({"ERROR": "statuses are not in TRUE"}, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, id):
        """
        ---
        request_serializer: MessageSerializer
        response_serializer: MessageSerializer
        """
        try:
            messages_ins = Message.objects.get(id=id, relation_to_job__user_relation=request.user.id)
        except Message.DoesNotExist as e:
            return Response({'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)
        messages_ins.delete()
        return Response(status=status.HTTP_200_OK)

    def update(self, request, id):
        """
        ---

        parameters:
            - name: chat_json
              paramType: array

        """
        request_data_dict = request.data
        if request_data_dict.get(
                'type') == 'user_accept_change_price' or 'user_dont_wont_change_price' or 'd_accept_change_price' or 'd_dont_wont_change_price':
            try:
                message_instance = Message.objects.get(Q(relation_to_job=id) &
                                                       Q(chat_participants__contains="'{0}'".format(
                                                           request.user.email)))
            except Message.DoesNotExist as e:
                return Response({_("ERROR occured in messages:  "): str(e)})
            id_message = int(request_data_dict.get('id'))
            value_set = {'active': 'false'}
            list_of_messages = message_instance.chat_json.get('list_of_messages')
            finding = [x for x in list_of_messages if x['id'] == id_message]
            if finding:
                find = finding[0]
                find.update(value_set)
                message_instance.save()
                serializer_message = MessageSerializer(data=find, instance=message_instance, partial=True)
                message_instance.save()
                job_instance = Jobs.objects.get(id=message_instance.relation_to_job.id)

                if request_data_dict.get('type') == 'user_accept_change_price':
                    work_flow_instance = WorkFlow.objects.get(relation_to_job=message_instance.relation_to_job)
                    work_flow_instance.pending_user_price = False
                    work_flow_instance.price = work_flow_instance.price_d
                    work_flow_instance.save()
                    data_for_chat = {"text_for_user": "{2} {0} {3} {1} UAH".format(
                        str(job_instance.relation_to_d.last_name),
                        str(work_flow_instance.price),
                        _('Dr'),
                        _('has changed price for ')),  # изменил цену на

                        "text_for_d": '{1} {0} UAH'.format(str(work_flow_instance.price),
                                                                _('You have changed the price of'))
                        # Вы изменили цену на
                    }
                    find.update(data_for_chat)
                    message_instance.save()

                    Group("chat-jobs-%s" % message_instance.relation_to_job.id).send({"text": json.dumps(find)})
                    # if not request.user.is_d:
                    #     event = EventManager(type='Message', action='user_accept_change_price', event_mark=message_instance.id,
                    #                          author=request.user,
                    #                          recepient=[message_instance.relation_to_job.relation_to_d.username],
                    #                          url='/d/discussion/{0}'.format(job_instance.id),
                    #                          string=''
                    #                          )
                    #     event.event_create()
                    event = EventManager(type='Message', action='user_accept_change_price',
                                         event_mark=message_instance.id,
                                         author=request.user,
                                         recepient=[message_instance.relation_to_job.relation_to_d],
                                         url='/d/discussion/{0}/'.format(job_instance.id),
                                         string='{0} {1} {3} - {2}'.format(
                                             str(
                                                 job_instance.relation_to_tr.relation_to_user.get_privacy_user_last_name),
                                             str(
                                                 job_instance.relation_to_t.relation_to_user.get_privacy_user_first_name),
                                             str(job_instance.relation_to_t.title),
                                             _('has accepted the terms of your price in the course of t')),
                                         # принял условия вашей цены в процессе лечения
                                         )
                    event.event_create()
                elif request_data_dict.get('type') == 'd_accept_change_price':
                    work_flow_instance = WorkFlow.objects.get(relation_to_job=message_instance.relation_to_job)
                    work_flow_instance.pending_dr_price = False
                    work_flow_instance.price = work_flow_instance.price_user
                    work_flow_instance.save()
                    data_for_chat = {
                        "text_for_user": '{1} {0} UAH'.format(work_flow_instance.price,
                                                              _('You have changed the price to ')),
                        # Вы изменили цену на
                        "text_for_dr": "{0} {2} {1} UAH".format(
                            str(job_instance.relation_to_t.relation_to_user.get_privacy_user_last_name),
                            work_flow_instance.price,
                            _('has changed price to'))}  # изменил цену на
                    find.update(data_for_chat)
                    message_instance.save()
                    Group("chat-jobs-%s" % message_instance.relation_to_job.id).send({"text": json.dumps(find)})

                    # if not request.user.is_d:
                    #     event = EventManager(type='Message', action='user_accept_change_price',
                    #                          event_mark=message_instance.id,
                    #                          author=request.user,
                    #                          recepient=[message_instance.relation_to_job.relation_to_d.username])
                    #     event.event_create()
                    # else:
                    event = EventManager(type='Message', action='d_accept_change_price',
                                         event_mark=message_instance.id,
                                         author=request.user,
                                         recepient=[
                                             message_instance.relation_to_job.relation_to_t.relation_to_user],
                                         url='/d/discussion/{0}/'.format(job_instance.id),
                                         string='{0} {1} {3} - {2}'.format(
                                             job_instance.relation_to_d.get_privacy_user_last_name,
                                             job_instance.relation_to_d.get_privacy_user_first_name,
                                             job_instance.relation_to_t.title,
                                             _('accepted the your terms of cost in the t process'))
                                         # принял условия вашей цены в процессе лечения
                                         )
                    event.event_create()

                elif request_data_dict.get('type') == 'user_dont_wont_change_price':
                    work_flow_instance = WorkFlow.objects.get(relation_to_job=message_instance.relation_to_job)
                    work_flow_instance.pending_user_price = False
                    data_for_chat = {'text_for_d': '{0} {2} {1} UAH'.format(
                        job_instance.relation_to_t.relation_to_user.get_privacy_user_last_name,
                        work_flow_instance.price_d,
                        _('has not accepted the price change')),  # не принял измение цены
                        "text_for_user": '{1} {0} UAH'.format(
                            work_flow_instance.price_d,
                            _('You did not accept price change'))
                    }
                    work_flow_instance.price_d = 0
                    work_flow_instance.save()
                    find.update(data_for_chat)
                    message_instance.save()
                    Group("chat-jobs-%s" % message_instance.relation_to_job.id).send({"text": json.dumps(find)})
                    event = EventManager(type='Message', action='user_dont_wont_change_price',
                                         event_mark=message_instance.id,
                                         author=request.user,
                                         recepient=[message_instance.relation_to_job.relation_to_d],
                                         url='/d/discussion/{0}/'.format(job_instance.id),
                                         string='{0} {1} {3} - {2}'.format(
                                             job_instance.relation_to_t.relation_to_user.get_privacy_user_last_name,
                                             job_instance.relation_to_t.relation_to_user.get_privacy_user_first_name,
                                             job_instance.relation_to_t.title,
                                             _('has refused to accept the terms of t price')
                                             # отказался принимать условия вашей цены в процессе лечения
                                         ))
                    event.event_create()
                elif request_data_dict.get('type') == 'd_dont_wont_change_price':
                    work_flow_instance = WorkFlow.objects.get(relation_to_job=message_instance.relation_to_job)
                    work_flow_instance.pending_d_price = False
                    data_for_chat = {'text_for_d': '{1} {0} UAH'.format(
                        work_flow_instance.price_user,
                        _('You did not accept price change')),
                        "text_for_user": '{2} {1} {3} {0} UAH'.format(
                            work_flow_instance.price_user,
                            work_flow_instance.relation_to_job.relation_to_d.last_name,
                            _('D'),
                            _('has not accepted the price change on the')  # не принял измение цены на
                        )
                    }
                    work_flow_instance.price_user = 0
                    work_flow_instance.save()
                    find.update(data_for_chat)
                    message_instance.save()
                    Group("chat-jobs-%s" % message_instance.relation_to_job.id).send({"text": json.dumps(find)})
                    event = EventManager(type='Message', action='d_dont_wont_change_price',
                                         event_mark=message_instance.id,
                                         author=request.user,
                                         recepient=[
                                             message_instance.relation_to_job.relation_to_t.relation_to_user],
                                         url='/d/discussion/{0}/'.format(job_instance.id),
                                         string='{0} {1} {3} - {2}'.format(
                                             job_instance.relation_to_d.get_privacy_user_last_name,
                                             job_instance.relation_to_d.get_privacy_user_first_name,
                                             job_instance.relation_to_t.title,
                                             _(
                                                 'has refused to accept the terms of your price in the course of t')
                                             # отказался принимать условия вашей цены в процессе лечения
                                         ))
                    event.event_create()
                if serializer_message.is_valid():
                    serializer_message.save()
                    return Response(serializer_message.data, status=status.HTTP_200_OK)
                return Response({"ERROR": _('in messages there is no message with id: %s ') % id_message})
            return Response({"ERROR": _('nothing is find')}, status=status.HTTP_400_BAD_REQUEST)
        return Response({"ERROR": _('data is not matching')}, status=status.HTTP_400_BAD_REQUEST)
