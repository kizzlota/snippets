# coding=utf-8
from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth import get_user_model
from django.contrib.postgres.fields import JSONField
import uuid
import os
from django.conf import settings
from django.contrib.postgres.fields import ArrayField
# from helpers import resize_logo
from versatileimagefield.fields import VersatileImageField, PPOIField
from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver
import datetime
from middleware.get_current_user import get_current_user
from binascii import hexlify
from os import urandom
from django.template.loader import render_to_string
from django.core.mail.message import EmailMultiAlternatives
from celery import shared_task
from celery.task import task
from django.core.cache import cache
from payments.models import Balance
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation
from clx.xms import Client
from clx.xms.api import MtBatchTextSmsCreate, MtBatchTextSmsResult
from phonenumber_field.modelfields import PhoneNumberField
from rest_framework.exceptions import APIException, ValidationError
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.db.models import Q
import random
import string
from storages.backends.sftpstorage import SFTPStorage, SFTPStorageFile
# Create your models here.

LANGUAGE_CHOICES = (
    ('uk', _('ukrainian')),
    ('ru', _('russian')),
    ('en', _('english')),
)


# class UserManager(BaseUserManager):
#     def create_user(self,  email, password, **extra_fields):
#         now = timezone.now()
#         if not email:
#             raise ValueError('The given Email must be set')
#         user = self.model(email=self.normalize_email(email),
#                           date_joined=now, **extra_fields)
#
#         user.set_password(password)
#         user.save(using=self._db)
#         return user
#
#     def create_superuser(self, email, password, **extra_fields):
#         user = self.create_user(email=email, password=password, **extra_fields)
#         user.is_staff = True
#         user.is_superuser = True
#         user.save(using=self._db)
#         return user


class UserManager(BaseUserManager):
    def create_user(self, email=None, username=None, phone_number=None, password=None, **extra_fields):
        now = timezone.now()
        if phone_number:
            user = self.model(phone_number=phone_number, last_login=now, date_joined=now, **extra_fields)
            user.set_password(password)
            user.save(using=self._db)
            return user
        # if not email:
        #     raise ValueError(_('The given email must be set'))
        if email:
            user = self.model(email=self.normalize_email(email),  # username=username,
                              last_login=now, date_joined=now, **extra_fields)
            # user.is_active = True
            user.set_password(password)
            user.save(using=self._db)
            return user
        else:
            raise ValidationError(_('Please check if you provide proper credentials'))

    def create_superuser(self, email, password, **extra_fields):
        user = self.create_user(email=email, password=password, **extra_fields)
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        return user


def get_file_path(self, filename):
    ext = filename.split('.')[-1]
    if not filename:
        filename = "%s.%s" % (uuid.uuid4(), ext)
    username = get_current_user().email
    return os.path.join(self.folder_name, filename)


class ImagesEveryone(models.Model):
    name = models.CharField(_('name'), max_length=255, blank=True, null=True)
    image = VersatileImageField('Picture', upload_to="pictures", blank=True, null=True,
                                width_field='width',
                                height_field='height', )  # default='defaultuserimage.jpg')
    height = models.PositiveIntegerField('Image Height', blank=True, null=True)
    width = models.PositiveIntegerField('Image Width', blank=True, null=True)
    ppoi = PPOIField('Image PPOI')

    class Meta:
        verbose_name = 'Everyone Images '
        verbose_name_plural = 'Pictures'

    def get_short_name(self):
        return self.name

    def __str__(self):
        return str(self.name)  # or u''


class UserFiles(models.Model):
    file = models.FileField(_('file'), blank=True, null=True, upload_to=get_file_path)
    date_of_add = models.DateField(_('date of adding'), auto_now_add=True, blank=True, null=True)
    name_file = models.CharField(_('name of the file'), blank=True, null=True, max_length=100)
    relation_to_user = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True, related_name='related_user',
                                         verbose_name=_('relation to user'))

    limit_choices_to = models.Q(app_label='profiles', model='useranalyzes') | models.Q(app_label='interrelation',
                                                                                       model='task')
    content_type = models.ForeignKey(ContentType, limit_choices_to=limit_choices_to, blank=True, null=True)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')
    capacity_content = models.IntegerField(default=0)

    def __str__(self):
        return str(self.name_file)  # or u''

    @property
    def folder_name(self):
        folder = 'user_files'
        return folder

    def file_url(self):
        if self.file:
            return self.file.url



class UserAzz(models.Model):
    title_analyzes = models.TextField(_('title of analyzes'), blank=True, null=True)
    
    date_of = models.DateField(_('date of analyzes'), auto_now=True)
    manual_date = models.DateTimeField(_('setting date manually'), blank=True, null=True, editable=True)
    everything_data = JSONField(_('everything data'), blank=True, null=True)
    # relation_to_files = models.ManyToManyField(UserFiles, blank=True, verbose_name=_('relation to files'))
    relation_to_user = models.ForeignKey('User', blank=True, null=True)
    files = GenericRelation(UserFiles)
    updated = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return (str(self.id) + ' ' + str(self.title_analyzes))  # or u''


class User(AbstractBaseUser, PermissionsMixin):
    # username = models.CharField(max_length=50, unique=True,
    #                             help_text=_(
    #                                 'Required. 30 characters or fewer. Letters, numbers and @/./+/-/_ characters'))
    email = models.EmailField(max_length=255, unique=True, verbose_name='e-mail', null=True, blank=True)
    phone_number = PhoneNumberField(blank=True, null=True)
   

    first_name = models.CharField(_('first name'), max_length=30, blank=True, null=True)
    last_name = models.CharField(_('last name'), max_length=30, blank=True, null=True)
    second_name = models.CharField(_('second name'), max_length=30, blank=True, null=True)

    is_staff = models.BooleanField(default=False,
                                   help_text=_('Designates whether the user can log into this admin site.'))
    is_active = models.BooleanField(default=True)

    

    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)
    privacy = models.BooleanField(_('privacy'), default=False)
    
    last_activity = models.DateTimeField(_('last activity'), null=True, blank=True)
    language = models.CharField(_('language'), max_length=20, blank=True, null=True, default='uk',
                                choices=LANGUAGE_CHOICES)

    confirmed = models.BooleanField(default=False)

    # Additions fields
    # social_img_url = models.CharField(max_length=120, blank=True, null=True)
    # profile_image = models.ImageField(upload_to="uploads", blank=False, null=False,
    #                                   default="/static/img/users/defaultuserimage.png")

    user_bio = models.TextField(_('user bio details'), max_length=1200, blank=True)

    personal_settings = JSONField(_('personal settings'),
                                  default={'_email_notification': 'true', '_email_notification': 'true',
                                           'email_notification': 'true', _email_notification': 'true',
                                           'summary_email_notification': 'true',
                                           'line_email_notification': 'true'})
    USERNAME_FIELD = 'email'
    # REQUIRED_FIELDS = ['first_name']

    objects = UserManager()

    def get_full_name(self):
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        return self.first_name

    def last_seen(self):
        return cache.get('seen_%s' % self.email)

    last_seen.short_description = _('user last seen')

    def online(self):
        if self.last_seen():
            now = datetime.datetime.now()
            if now > self.last_seen() + datetime.timedelta(
                    seconds=settings.USER_ONLINE_TIMEOUT):
                return False
            else:
                return True
        else:
            return False

    online.short_description = _('user online')

    @property
    def get_privacy_user_first_name(self):
        if not self.is_doc:
            if self.privacy:
                return _("anonymous_name")
            else:
                return self.first_name
        else:
            return self.first_name

    @property
    def get_privacy_user_last_name(self):
        if not self.is_doc:
            if self.privacy:
                return _("anonymous_last_name ")
            else:
                return self.last_name
        else:
            return self.last_name

    @property
    def get_privacy_user_second_name(self):
        if not self.is_doc:
            if self.privacy:
                return _("anonymous user_second_name")
            else:
                return self.second_name
        else:
            return self.second_name

    # def email_user(self, subject, message, from_email=None):
    #    send_mail(subject, message, from_email, [self.email])

    def __str__(self):
        if self.email:
            return str(self.email)  # or u''
        elif self.phone_number:
            return str(self.phone_number)




class UserBioDetails(models.Model):
    SEX_CHOICES = ((1, _('male')),
                   (2, _('female')))
    BLOOD_CHOICES = (
        (1, u'O(I)Rh(−)'),
        (2, u'O(I)Rh(+)'),
        (3, u'A(II)Rh(-)'),
        (4, u'A(II)Rh(+)'),
        (5, u'B(III)Rh(-)'),
        (6, u'B(III)Rh(+)'),
        (7, u'AB(IV)Rh(-)'),
        (8, u'AB(IV)Rh(+)'),
    )
    DISABILITY_CHOICES = (
        (1, u'no'),
        (2, u'I'),
        (3, u'II'),
        (4, u'III'),
    )
    balance = models.IntegerField(_('balance'), blank=True, null=True, default=0)
    *** = models.IntegerField(_(***), blank=True, null=True, choices=SEX_CHOICES)
    *** = models.DateField(_(**'), blank=True, null=True)
    telephone_number = models.CharField(_('phone number'), max_length=15, blank=True, null=True)
    address = models.CharField(_('address'), max_length=250, blank=True, null=True)
    *** = models.IntegerField(_('***'), blank=True, null=True, choices=DISABILITY_CHOICES)
    *** = models.IntegerField(_('***'), blank=True, null=True, choices=BLOOD_CHOICES)
    *** = models.NullBooleanField(_(***'), blank=True, null=True)
    *** = models.NullBooleanField(_('***'), blank=True, null=True)
    *** = ArrayField(models.TextField(max_length=500, blank=True, null=True), blank=True, null=True)
    *** = ArrayField(models.TextField(max_length=500, blank=True, null=True), blank=True, null=True)
    *** = ArrayField(models.TextField(max_length=500, blank=True, null=True), blank=True, null=True)
    *** = ArrayField(models.TextField(max_length=500, blank=True, null=True), blank=True, null=True)
    *** = ArrayField(models.TextField(max_length=500, blank=True, null=True), blank=True, null=True)
    *** = ArrayField(models.TextField(max_length=500, blank=True, null=True), blank=True, null=True)
    *** = ArrayField(models.TextField(max_length=500, blank=True, null=True), blank=True, null=True)
    *** = models.IntegerField(_('***'), blank=True, null=True)
    *** = models.IntegerField(_('***'), blank=True, null=True)
    *** = models.TextField(_('** ***'), max_length=200, blank=True, null=True)
    *** = models.TextField(_('***'), max_length=200, blank=True, null=True)
    *** = models.TextField(_('***'), max_length=200, blank=True, null=True)
    ***s = models.TextField(_('***'), max_length=500, blank=True, null=True)
    city = models.CharField(_('city'), max_length=150, blank=True, null=True)
    street = models.CharField(_('street'), max_length=150, blank=True, null=True)
    country = models.CharField(_('country'), max_length=50, blank=True, null=True)
    relation_to_images = models.ManyToManyField(ImagesEveryone, blank=True, default='defaultuserimage.jpg',
                                                verbose_name=_('relation to images'))
    relation_to_user = models.ForeignKey(User, verbose_name=_('relation field to User model by FK'))

    def __str__(self):
        return str(self.id)  # or u''


class UserPhoto(models.Model):
    name = models.CharField(max_length=255, blank=True, null=True)
    image = VersatileImageField('Image', upload_to="uploads", blank=True, null=True,
                                width_field='width',
                                height_field='height',
                                default='defaultuserimage.jpg')
    height = models.PositiveIntegerField('Image Height', blank=True, null=True)
    width = models.PositiveIntegerField('Image Width', blank=True, null=True)
    relation_to_user = models.ForeignKey(User, related_name='userphotos', verbose_name=_('relation to user'))
    ppoi = PPOIField('Image PPOI')

    class Meta:
        verbose_name = 'User Photo name from Meta'
        verbose_name_plural = 'Image User Photo name from Meta'

    def get_short_name(self):
        return self.name

    def __str__(self):
        return str(self.name)  # or u''


class ***Categories(models.Model):
    name = JSONField(_('name of category'), blank=True, null=True)

    def __str__(self):
        return str(self.id)  # or u''


class ***Profile(models.Model):
    language = models.CharField(_('language'), max_length=100, blank=True, null=True)
    country = models.CharField(_('country'), max_length=100, blank=True, null=True)
    phone_number = models.CharField(_('phone number'), max_length=100, blank=True, null=True)
    *** = models.CharField(_(***'), max_length=200, blank=True, null=True)
    *** = models.CharField(_('***'), max_length=200, blank=True, null=True)
    description = models.TextField(_('description'), max_length=1000, blank=True, null=True)
    *** = models.TextField(_('***'), max_length=1000, blank=True, null=True)
    *** = models.FloatField(_('***'), default=0)
    *** = models.IntegerField(_('***'), default=0)
    *** = models.IntegerField(_('***'), default=0)
    *** = models.CharField(_('***'), blank=True, null=True, max_length=1500)
    *** = JSONField(_('***'), blank=True, null=True)
    additional_info = JSONField(_('additional info'), blank=True, null=True)
    additional_files = models.FileField(_('additional files'), blank=True, null=True, upload_to=get_file_path)
    relation_to_user = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True, related_name='related_doctor',
                                         verbose_name=_('relation to user'))
    relation_to_images = models.ManyToManyField(ImagesEveryone, blank=True, verbose_name=_('relation to images'))
    *** = models.ManyToManyField(DocCategories, verbose_name=_(***'))

    def relation_to_doctor_cats(self):
        return self.relation_to_doc_categories.all()

    @property
    def doc_category(self):
        return None

    @property
    def folder_name(self):
        folder = 'user_images'
        return folder

    def __str__(self):
        return str(self.relation_to_user.email)  # or u''


#

@receiver(post_save, sender=User)
def create_doc_profile(instance, created, **kwargs):
    if created:
        photo = UserPhoto.objects.create(relation_to_user=instance)
        photo.save()
        if not instance.is_not_doc_yet:
            ***.objects.create(relation_to_user=instance)

        else:
            ***.objects.create(relation_to_user=instance)
        Balance.objects.create(owner=instance)


def _generate_code():
    return hexlify(urandom(30))


class SignupCodeManager(models.Manager):
    def create_signup_code(self, user, email, ipaddr):
        code = _generate_code()
        signup_code = self.create(user=user, email=email, code=code, ipaddr=ipaddr)
        return signup_code

    def set_email_is_confirmed(self, code):
        try:
            signup_code = SignupCode.objects.get(code=code)
            signup_code.user.email = signup_code.email
            signup_code.user.email_confirmed = True
            signup_code.user.save()
            return True
        except SignupCode.DoesNotExist:
            return False


class PasswordResetCodeManager(models.Manager):
    def create_reset_code(self, user, ipaddr):
        code = _generate_code()
        password_reset_code = self.create(user=user, code=code, ipaddr=ipaddr)

        return password_reset_code


class AbstractBaseCode(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    email = models.EmailField(max_length=255, null=True)
    code = models.CharField(_('code'), max_length=60, primary_key=True)
    confirmed = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        abstract = True

    @staticmethod
    def send_email(parameters, prefix):
        subject_file = 'mail/%s_subject.txt' % prefix
        txt_file = 'mail/%s.txt' % prefix
        html_file = 'mail/%s.html' % prefix

        subject = render_to_string(subject_file).strip()
        from_email = settings.DEFAULT_EMAIL_FROM
        to = parameters.get('email')

        # Make some context available
        ctxt = {
            'email': to,
            'username': parameters.get('username'),
            'code': parameters.get('code')
        }

        html_content = render_to_string(html_file, ctxt)
        msg = EmailMultiAlternatives(subject, html_content, from_email, [to])
        # bcc=[bcc_email])
        msg.content_subtype = "html"
        msg.send()
        # try:
        #     msg.send()
        # except Exception as exc:
        #     raise SignupCode.send_signup_email.retry(exc=exc)

    def __str__(self):
        return self.code


class SignupCode(AbstractBaseCode):
    ipaddr = models.GenericIPAddressField(_('ip address'))
    objects = SignupCodeManager()

    @staticmethod
    @task(default_retry_delay=3 * 20, max_retries=3)
    def send_signup_email(parameters):
        # print self.get('email')
        prefix = 'signup_email'
        if parameters.get('doctor'):
            prefix = 'signup_email_doctor'

        AbstractBaseCode.send_email(parameters, prefix=prefix)

    @staticmethod
    @task(default_retry_delay=3 * 20, max_retries=3)
    def send_welcome_email(parameters):
        # print self.get('email')
        prefix = 'welcome_email'
        if parameters.get('doctor'):
            prefix = 'welcome_email_doctor'
        AbstractBaseCode.send_email(parameters, prefix=prefix)


def sms_code_generator():
    my_randoms = ''.join(random.choice(string.digits) for _ in range(5))
    print('gebarted by random method %s' % my_randoms)
    return my_randoms


class SmsCodeManager(models.Manager):

    def create_sms_code(self, user, phone_number=None, email=None):
        code = sms_code_generator()
        _code = None
        if phone_number:
            _code = self.create(user=user, phone_number=phone_number, code=code)
        if email:
            _code = self.create(user=user, email=email, code=code)
        # print(sms_code.code)
        # return ''.join(sms_code.code)
        return _code

    def regular_sms_code(self, user, phone_number=None, email=None):
        code = sms_code_generator()
        _code = None
        if phone_number:
            code_existance = SmsPasswordResetCode.objects.filter(Q(user=user) | Q(phone_number=phone_number))
            if not code_existance.exists():
                _code = SmsPasswordResetCode.objects.create(user=user, phone_number=phone_number, code=code)
        if email:
            _code = SmsPasswordResetCode.objects.create(user=user, email=email, code=code)
        return _code

    def set_email_is_confirmed(self, code):
        try:
            signup_code = SmsCode.objects.get(code=code)
            signup_code.user.email = signup_code.email
            signup_code.user.email_confirmed = True
            signup_code.user.save()
            return True
        except SmsCode.DoesNotExist:
            return False


class AbstactCode(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    phone_number = PhoneNumberField(blank=True, null=True)
    email = models.EmailField(max_length=150, null=True, blank=True)
    code = models.CharField(_('code'), max_length=60, default='12345')
    confirmed = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    confirmed_at = models.DateTimeField(auto_now=True)
    batch_id = models.CharField(max_length=100, blank=True, null=True)
    try_counter = models.IntegerField(default=0)
    active = models.BooleanField(default=True)

    objects = SmsCodeManager()

    class Meta:
        abstract = True

    @staticmethod
    def send_email(parameters, prefix):
        subject_file = 'mail/%s_subject.txt' % prefix
        txt_file = 'mail/%s.txt' % prefix
        html_file = 'mail/%s.html' % prefix

        subject = render_to_string(subject_file).strip()
        from_email = settings.DEFAULT_EMAIL_FROM
        to = parameters.get('email')

        # Make some context available
        ctxt = {
            'email': to,
            'username': parameters.get('username'),
            'code': parameters.get('code')
        }

        html_content = render_to_string(html_file, ctxt)
        msg = EmailMultiAlternatives(subject, html_content, from_email, [to])
        # bcc=[bcc_email])
        msg.content_subtype = "html"
        msg.send()

    @staticmethod
    @task(default_retry_delay=3 * 20, max_retries=3)
    def send_signup_email(parameters):
        prefix = 'signup_email'
        if parameters.get('doctor'):
            prefix = 'signup_email_doctor'
        SmsCode.send_email(parameters=parameters, prefix=prefix)

    @staticmethod
    @task(default_retry_delay=3 * 20, max_retries=3)
    def send_reset_password_email(parameters):
        prefix = 'recover_account'
        if parameters.get('doctor'):
            prefix = 'recover_account_doctor'
        SmsCode.send_email(parameters=parameters, prefix=prefix)


class SmsCode(AbstactCode):
   

    # objects = SmsCodeManager()

    # @task(default_retry_delay=3 * 20, max_retries=3)
    @staticmethod
    def send_sms(parameters):
        local_username_clx = 'testmedici12'
        local_token_clx = '149ef86195a04f748f325359503a502a'
        username = getattr('settings', 'SMS_USERNAME', local_username_clx)
        token = getattr('settings', 'SMS_API_TOKEN', local_token_clx)
        code = parameters.get('code', None)
        user = parameters.get('user', None)
        phone_number = str(parameters.get('phone_number', None))
        if username and token:
            sms_client = Client(username, token)
            batch_params = MtBatchTextSmsCreate()
            batch_params.sender = 'Medician'
            batch_params.recipients = {phone_number}
            batch_params.body = code
            result = sms_client.create_batch(batch_params)
            print('batch is ----> %s' % result.batch_id)
            return result.batch_id

    @staticmethod
    def send_email(parameters, prefix):
        subject_file = 'mail/%s_subject.txt' % prefix
        txt_file = 'mail/%s.txt' % prefix
        html_file = 'mail/%s.html' % prefix

        subject = render_to_string(subject_file).strip()
        from_email = settings.DEFAULT_EMAIL_FROM
        to = parameters.get('email')

        # Make some context available
        ctxt = {
            'email': to,
            'username': parameters.get('email', None),
            'code': parameters.get('code')
        }

        html_content = render_to_string(html_file, ctxt)
        msg = EmailMultiAlternatives(subject, html_content, from_email, [to])
        # bcc=[bcc_email])
        msg.content_subtype = "html"
        msg.send()

    @staticmethod
    @task(default_retry_delay=3 * 20, max_retries=3)
    def send_signup_email(parameters):
        prefix = 'signup_email'
        if parameters.get('***'):
            prefix = 'signup_email_***'
        SmsCode.send_email(parameters=parameters, prefix=prefix)

    @staticmethod
    def send_test_email(parameters):
        print('try send email')
        prefix = 'signup_email'
        if parameters.get('***'):
            prefix = 'signup_email_***'
        SmsCode.send_email(parameters=parameters, prefix=prefix)

    @staticmethod
    @task(default_retry_delay=3 * 20, max_retries=3)
    def send_welcome_email(parameters):
        # print self.get('email')
        prefix = 'welcome_email'
        if parameters.get('***'):
            prefix = 'welcome_email_***'
        SmsCode.send_email(parameters, prefix=prefix)

    def __str__(self):
        return str(self.code)


@receiver(post_save, sender=SmsCode)
def email_update_after_confirmation(instance, created, **kwargs):
    if instance.confirmed:
        email = instance.user.email
        if email and not instance.user.phone_number:
            if '__' in email:
                if isinstance(email, str):
                    splitted_email = email.split('__')
                    clenead_email = splitted_email[-1]
                    instance.user.email = clenead_email
                    instance.user.save()


class SmsPasswordResetCode(AbstactCode):
    @staticmethod
    def create_reset_sms_code(user, phone_number=None, email=None):
        code = sms_code_generator()
        _code = None
        if phone_number:
            code_existance = SmsPasswordResetCode.objects.filter(Q(user=user) | Q(phone_number=phone_number))
            if code_existance.exists():
                print('update')
                update_code = SmsPasswordResetCode.objects.update(user=user, phone_number=phone_number, code=code)
                _code = SmsPasswordResetCode.objects.filter(Q(user=user) | Q(phone_number=phone_number)).first()
                print('code code is {0}'.format(_code.code))
            else:
                _code = SmsPasswordResetCode.objects.create(user=user, phone_number=phone_number, code=code)
        if email:
            _code = SmsPasswordResetCode.objects.create(user=user, email=email, code=code)
        return _code

    @staticmethod
    def send_reset_code(parameters):
        local_username_clx = '****'
        local_token_clx = '****'
        username = getattr('settings', 'SMS_USERNAME', local_username_clx)
        token = getattr('settings', 'SMS_API_TOKEN', local_token_clx)
        code = parameters.get('code', None)
        user = parameters.get('user', None)
        phone_number = str(parameters.get('phone_number', None))
        if username and token:
            sms_client = Client(username, token)
            batch_params = MtBatchTextSmsCreate()
            batch_params.sender = 'Medician'
            batch_params.recipients = {phone_number}
            batch_params.body = 'Reset password code is: {0}'.format(code)
            result = sms_client.create_batch(batch_params)
            print('batch is ----> %s' % result.batch_id)
            return result.batch_id
 

class PasswordResetCode(AbstractBaseCode):
    ipaddr = models.GenericIPAddressField(_('ip address'))
    objects = PasswordResetCodeManager()

    @staticmethod
    @task(default_retry_delay=3 * 20, max_retries=3)
    def send_password_reset_email(parameters):
        prefix = 'password_reset_email'
        AbstractBaseCode.send_email(parameters, prefix=prefix)


class MailTaskLog(models.Model):
    task_id = models.CharField(max_length=80, editable=False, db_index=True)
    task_status = models.CharField(max_length=20, db_index=True)
    task_args = models.TextField(_('Arguments'), null=True, blank=True)
    task_kwargs = models.TextField(_('Keyword arguments'), null=True, blank=True)
    sending_error = models.TextField(_('Sending error message'), null=True, blank=True)
    celery_error = models.TextField(null=True, blank=True)
    num_of_retries = models.PositiveIntegerField(default=1)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name=_('User'), null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)

    @classmethod
    def cleanup(cls, days=7):
        date = datetime.datetime.now() - datetime.timedelta(days=days)
        cls.objects.filter(created__lte=date).delete()

    def __unicode__(self):
        return self.task_status

    class Meta:
        verbose_name = _('Mail task log')
        verbose_name_plural = _('Mail task logs')


class LawAgreement(models.Model):
    relation_to_text = models.ManyToManyField('TextAgrement', blank=True, related_name='agreement_texts')
    relation_to_user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='agreement', on_delete=models.CASCADE,
                                         blank=True, null=True)
    user_agreement = models.BooleanField(default=False)
    user_adult = models.BooleanField(default=False)
    updated = models.DateTimeField(auto_now=True)
    timestamp = models.DateTimeField(auto_now_add=True)
    privacy_agreement = models.BooleanField(default=False)
    terms_agreement = models.BooleanField(default=False)

    def __str__(self):
        if self.relation_to_user:
            return str(str(self.id) + str(self.relation_to_user))
        else:
            return str(self.id)


class TextAgrement(models.Model):
    title = models.CharField(max_length=100, blank=True, null=True)
    text_agreement = models.TextField(blank=True, null=True)

    def __str__(self):
        if self.title:
            return self.title

#
# @receiver(post_save, sender=User)
# def agreement_save(instance, created, **kwargs):
#     if instance:
#
#         agreement_instnance = instance.agreement.all()
#         print('Agrement {0}'.format(agreement_instnance.__dict__))
