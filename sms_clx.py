from .models import SmsCode, SignupCode
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated, AllowAny, IsAdminUser
from profiles.serializers import *
from rest_framework.response import Response
from rest_framework import status
from datetime import timedelta
from profiles.models import *
from rest_framework import generics
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth import update_session_auth_hash
from rest_framework.authtoken.models import Token
from django.db.models import Q
from clx.xms import Client
import json
import yaml
import requests
from .serializers import SmsCodeSerializer
from django.shortcuts import HttpResponseRedirect, redirect
from rest_framework.reverse import reverse, reverse_lazy
import datetime
from profiles.serializers import AccountSerializer

# @task(default_retry_delay=3 * 20, max_retries=3)
def is_delivered(batch_id):
    try:
        sms_code = SmsCode.objects.get(batch_id=batch_id)
    except SmsCode.DoesNotExist as e:
        return Response({"ERROR": str(e)}, status=status.HTTP_400_BAD_REQUEST)
    token = '****'
    username = '***'
    headers = {'Authorization': "Bearer {0}".format(token),
               'content-type': "application/json",
               }
    endpoint = "https://api.clxcommunications.com/xms/v1/{service_plan_id}/batches/{batch_id}/delivery_report".format(
        service_plan_id=username, batch_id=batch_id)
    result = requests.get(url=endpoint, headers=headers)
    return result


class SmsCodeHandler(viewsets.ViewSet):
    permission_classes = (AllowAny,)

    def post(self, request):
        code = request.data.get('code', None)
        phone = request.data.get('phone_number', None)
        email = request.data.get('email', None)
        try:
            if email:
                sms_instanse = SmsCode.objects.get(user__email=email, active=True)
            else:
                sms_instanse = SmsCode.objects.get(phone_number=phone, active=True)
        except SmsCode.DoesNotExist as e:
            return Response({"ERROR": str(e)}, status=status.HTTP_400_BAD_REQUEST)

        sms_instanse.try_counter += 1
        sms_instanse.save()
        time_to_expire = sms_instanse.created_at + timedelta(hours=1)
        now = timezone.now()
        if not now > time_to_expire:
            if not sms_instanse.try_counter > 4:
                if not sms_instanse.confirmed:
                    if code:
                        if int(sms_instanse.code) == int(code):
                            sms_instanse.confirmed = True
                            sms_instanse.save()

                            check_user = sms_instanse.user
                            check_user.confirmed = True
                            check_user.save()

                            if sms_instanse.user:
                                sms_instanse.user.backend = 'django.contrib.auth.backends.ModelBackend'
                                try:
                                    token, created = Token.objects.get_or_create(user=sms_instanse.user)
                                except Token.DoesNotExist as e:
                                    return Response({'ERROR': str(e)}, status=status.HTTP_400_BAD_REQUEST)

                                serializer = AccountSerializer(instance=sms_instanse.user, remove_fields=['user_photo'])
                                if serializer:
                                    response = Response(serializer.data, status=status.HTTP_200_OK)
                                    response.set_cookie('token', token.key)
                                    sms_instanse.active = False
                                    return response
                                return Response({"ERROR": _('error occured in provided data')},
                                                status=status.HTTP_400_BAD_REQUEST)
                            return Response({"ERROR": _('You are not confirmed')}, status=status.HTTP_200_OK)
                        return Response({"ERROR": _('code is mismatch'), "status": False})
                    return Response({"ERROR": _('Please enter the code sent by sms')},
                                    status=status.HTTP_400_BAD_REQUEST)
                return Response({"ERROR": _('You are confirmed already')}, status=status.HTTP_303_SEE_OTHER)
            sms_instanse.delete()
            return Response(
                {"ERROR": _('You try more than 3 times, need to resend sms. And your code has been deleted')},
                status=status.HTTP_400_BAD_REQUEST)
        return Response({"ERROR": _('Your confirmation code is expires, you need to resend the code')},
                        status=status.HTTP_400_BAD_REQUEST)

    def update(self, request):
        if not request.user.is_authenticated():
            phone_number = request.data.get('phone_number', None)
            email = request.data.get('email', None)
            print(phone_number)
            print(email)
            try:
                if phone_number and not email:
                    print('lolLLL')
                    sms_instanse = SmsCode.objects.get(phone_number=phone_number, active=True)
                elif email and not phone_number:
                    sms_instanse = SmsCode.objects.get(user__email=email, active=True)
                # sms_instanse = SmsCode.objects.get(Q(phone_number=phone_number) | Q(email=email) | Q(user__email=email))
            except SmsCode.DoesNotExist as e:
                return Response({"ERROR": "cant find code for for provided credentials"})
            user = sms_instanse.user
            sms_instanse.delete()
            # [sms.delete() for sms in sms_instanse]
            if email:
                if '__' in email:
                    if isinstance(email, str):
                        splitted_email = email.split('__')
                        email_for_send = splitted_email[-1]
            # sms_code = SmsCode.objects.create_sms_code(Q(user=user) & (Q(phone_number=phone_number) | Q(email=email_for_send)))
            if phone_number and not email:
                sms_code = SmsCode.objects.create_sms_code(user=user, phone_number=phone_number)
                parameters = {'user': user, 'phone_number': phone_number, 'code': sms_code.code}
            if email and not phone_number:
                sms_code = SmsCode.objects.create_sms_code(user=user, email=email_for_send)
                parameters = {'user': user, 'email': email_for_send, 'code': sms_code.code}

            # batch = sms_code.send_sms(parameters=parameters)
            # sms_code.batch_id = batch
            # sms_code.save()
            # print(sms_code.code)
            serializer = SmsCodeSerializer(instance=sms_code)
            if serializer:
                return Response(serializer.data, status=status.HTTP_200_OK)
        return Response({'ERROR': _('seems You don\'t need to be authorized')}, status=status.HTTP_400_BAD_REQUEST)

    def fetch_batch_id(self, request):
        if request.user.is_authenticated():
            try:
                sms_instanse = SmsCode.objects.get(user=request.user)
            except SmsCode.DoesNotExist as e:
                return Response({"ERROR": str(e)}, status=status.HTTP_400_BAD_REQUEST)
            if sms_instanse.batch_id:
                local_username_clx = 'testmedici12'
                local_token_clx = '149ef86195a04f748f325359503a502a'
                username = getattr('settings', 'SMS_USERNAME', local_username_clx)
                token = getattr('settings', 'SMS_API_TOKEN', local_token_clx)
                sms_client = Client(username, token)
                result = sms_client.fetch_batch(sms_instanse.batch_id)
                str_to_yaml = str(result.__dict__)
                data = yaml.load(str_to_yaml)
                if result:
                    return Response(data, status=status.HTTP_200_OK)
            return Response({"ERROR": _('please provide sms id')})
        return Response({'ERROR': _('You need to be authorized')}, status=status.HTTP_400_BAD_REQUEST)


class SmsCodeChecker(viewsets.ViewSet):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        try:
            sms = SmsCode.objects.get(user=request.user)
        except SmsCode.DoesNotExist as e:
            return Response({'ERROR': str(e)}, status=status.HTTP_400_BAD_REQUEST)
        except SmsCode.MultipleObjectsReturned as e:
            return Response({'ERROR': str(e)}, status=status.HTTP_400_BAD_REQUEST)
        sms_status = is_delivered(sms.batch_id)
        return Response(data=sms_status, status=status.HTTP_200_OK)


class EmailConfirmationChecker(viewsets.ViewSet):
    permission_classes = (IsAuthenticated,)

    def retrieve(self, request):
        email_signup_instance = SignupCode.objects.get(user=request.user)
        if email_signup_instance.confirmed:
            return Response({'OK', _('email is confirmed')}, status=status.HTTP_200_OK)
        return Response({"ERROR": _('email is not confirmed')}, status=status.HTTP_400_BAD_REQUEST)
